package db

import (
	"fmt"
	"log"

	"github.com/globalsign/mgo/bson"

	"github.com/globalsign/mgo"
	"github.com/spf13/viper"
)

type Manager interface {
	GetAllProjects() ([]map[string]string, error)
	GetProject(name string) (map[string]string, error)
	CreateProject(name string, Code string) error
	UpdateProject(name string, code string) error
	DeleteProject(name string) error
}

type Database struct {
	session  *mgo.Session
	projects *mgo.Collection
}

func New() *Database {
	var db = Database{}
	var err error
	url := viper.GetString("db.url")
	db.session, err = mgo.Dial(url)
	if err != nil {
		log.Printf("Database init error: %v\n", err.Error())
	}
	if !viper.IsSet("db.database") {
		viper.SetDefault("db.database", "Clerk")
	}
	dbName := viper.GetString("db.database")
	db.projects = db.session.DB(dbName).C("projects")

	index := mgo.Index{
		Key:        []string{"name"},
		Unique:     true,
		DropDups:   true,
		Background: true,
		Sparse:     true,
	}

	err = db.projects.EnsureIndex(index)
	if err != nil {
		log.Fatal(err)
	}
	return &db
}

func (d *Database) CreateProject(Name string, Code string) error {
	//var result []interface{}
	var data = map[string]interface{}{
		"name": Name,
		"code": Code,
	}

	err := d.projects.Insert(data)
	if err != nil {
		return err
	}
	return nil
}

func (d *Database) UpdateProject(name string, code string) error {
	var result []interface{}
	var data = map[string]interface{}{
		"name": name,
	}
	query := d.projects.Find(data)
	err := query.All(&result)
	if len(result) == 0 {
		return fmt.Errorf("Project with this name not be found")
	}

	project := map[string]interface{}(result[0].(bson.M))
	project["code"] = code
	err = d.projects.UpdateId(project["_id"], project)
	if err != nil {
		return err
	}
	return nil
}

func (d *Database) GetAllProjects() ([]map[string]string, error) {
	var res []map[string]string
	var f = make(map[string]interface{})
	var data []interface{}
	err := d.projects.Find(f).All(&data)
	if err != nil {
		return nil, err
	}
	for i, bsonProject := range data {
		res = append(res, make(map[string]string))
		project := map[string]interface{}(bsonProject.(bson.M))

		name, ok := project["name"]
		if !ok {
			log.Printf("Project with guid '%v' haven't name", project["_id"])
		}
		code, ok := project["code"]
		if !ok {
			log.Printf("Project with guid '%v' haven't code", project["_id"])
		}
		res[i]["name"] = name.(string)
		res[i]["code"] = code.(string)
	}

	return res, nil
}

func (d *Database) GetProject(name string) (map[string]string, error) {
	var result []interface{}
	var data = map[string]interface{}{
		"name": name,
	}
	query := d.projects.Find(data)
	err := query.All(&result)
	if err != nil {
		return nil, err
	}
	if len(result) == 0 {
		return nil, fmt.Errorf("Project with this name not exists")
	}

	var projStr = make(map[string]string)
	proj := map[string]interface{}(result[0].(bson.M))
	projStr["name"] = proj["name"].(string)
	projStr["code"] = proj["code"].(string)

	return projStr, nil
}

func (d *Database) DeleteProject(name string) error {
	var data = map[string]interface{}{
		"name": name,
	}
	_, err := d.projects.RemoveAll(data)
	return err
}
