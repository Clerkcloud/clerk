package db

import (
	"testing"

	"github.com/spf13/viper"
)

func TestConfig(t *testing.T) {
	viper.SetConfigFile("test_conf.yaml")
	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		t.Fatalf("Fatal error config file: %s \n", err)
	}
	viper.WatchConfig()
	if viper.GetString("db.database") != "ClerkTest" {
		t.Fatalf("expected 'ClerkTest', got='%s'", viper.GetString("db.database"))
	}
}

func TestCRUDProject(t *testing.T) {
	viper.SetConfigFile("test_conf.yaml")
	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		t.Fatalf("Fatal error config file: %s \n", err)
	}
	viper.WatchConfig()
	var d = New()
	var name = "test"
	var code = `For model{}
	//route/{param} GET {
		field1 string	
		field2 string
	}(
		1+1
	)
	`
	var codeUpd = `For model{}
	//route/{param} GET {
		field1 string	
		field3 string
	}(
		1+1
	)
	`
	var inter interface{}
	d.projects.RemoveAll(inter)
	testCreate(t, d, name, code)
	testSelect(t, d, name, code)
	testUpdate(t, d, name, codeUpd)
	testSelect(t, d, name, codeUpd)
	testDelete(t, d, name)
}

func testCreate(t *testing.T,
	d *Database,
	name string,
	code string) {
	err := d.CreateProject(name, code)
	if err != nil {
		t.Errorf("test create: %v", err)
	}
}

func testUpdate(t *testing.T,
	d *Database,
	name string,
	code string) {
	err := d.UpdateProject(name, code)
	if err != nil {
		t.Errorf("test update: %v", err)
	}
}

func testSelect(t *testing.T,
	d *Database,
	name string,
	code string) {
	sel, err := d.GetProject(name)
	if err != nil {
		t.Errorf("test select: %v", err)
	}
	selCode, ok := sel["code"]
	if !ok {
		t.Fatal("Haven't code")
	}
	if selCode != code {
		t.Errorf("Bad code value, expected:\n%s\ngot:%s", code, sel["code"])
	}
	if sel["name"] != name {
		t.Errorf("Bad name value, expected:\n%s\ngot:%s", name, sel["name"])
	}
}

func testDelete(t *testing.T,
	d *Database,
	name string) {
	err := d.DeleteProject(name)
	if err != nil {
		t.Errorf("test delete: %v", err)
	}
}

func TestGetAllProject(t *testing.T) {
	viper.SetConfigFile("test_conf.yaml")
	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		t.Fatalf("Fatal error config file: %s \n", err)
	}
	viper.WatchConfig()
	var d = New()
	var inter interface{}
	d.projects.RemoveAll(inter)
	var count = 5
	var code = `For model{}
	//route/{param} GET {
		field1 string	
		field2 string
	}(
		1+1
	)
	`
	for i := 0; i < count; i++ {
		si := string(i)
		d.CreateProject(si+"name", code)
		if err != nil {
			t.Fatal(err)
		}
	}

	res, err := d.GetAllProjects()
	if err != nil {
		t.Fatalf("can`t read all projects: %v", res)
	}

	if len(res) != count {
		t.Fatalf("Projects count expected=%v, got=%v", count, len(res))
	}

	for i := 0; i < count; i++ {
		if res[i]["code"] != code {
			t.Errorf("Bad value of code, expected:\n%v\ngot:\n%v", code, res[i]["code"])
		}
	}
}
