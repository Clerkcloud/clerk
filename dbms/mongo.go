package dbms

import (
	"fmt"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

type Mongo struct {
	session     *mgo.Session
	projectName string
	entityName  string
}

func NewMongo(url string, projectName string, entityName string) (*Mongo, error) {
	var mongo Mongo
	var err error
	mongo.session, err = mgo.Dial(url)
	mongo.projectName = projectName
	mongo.entityName = entityName
	return &mongo, err
}

func (m *Mongo) c() *mgo.Collection {
	return m.session.DB(m.projectName).C(m.entityName)
}

//guidToId меняет параметр guid на _id для mongoDB
func guidToID(data *map[string]interface{}) {
	val, ok := (*data)["guid"]
	if !ok {
		return
	}
	delete(*data, "guid")
	switch v := val.(type) {
	/*
		case []string:
			var ids = make([]bson.ObjectId, len(v), len(v))
			for i, l := range v {
				ids[i] = bson.ObjectId(l)
			}
			(*data)["_id"] = ids
	*/
	case string:
		(*data)["_id"] = bson.ObjectIdHex(v)
	}
	return
}

func getNormalState(data []interface{}) ([]map[string]interface{}, error) {
	var result = make([]map[string]interface{}, 0, len(data))
	for i, val := range data {
		switch v := val.(type) {
		case bson.M:
			res := map[string]interface{}(val.(bson.M))
			idToGUID(&res)
			result = append(result, res)
		default:
			return result, fmt.Errorf("el №%v, expected slice, get %+v", i, v)
		}
	}
	return result, nil
}

func idToGUID(data *map[string]interface{}) {
	val, ok := (*data)["_id"]
	if !ok {
		return
	}
	objIDHex := val.(bson.ObjectId)
	delete(*data, "_id")
	(*data)["guid"] = objIDHex.Hex()
	return
}

func (m *Mongo) TransactionSelect(d map[string]interface{}) ([]map[string]interface{}, error) {
	var result []interface{}
	guidToID(&d)
	query := m.c().Find(d)
	err := query.All(&result)
	normResult, err := getNormalState(result)
	return normResult, err
}

func (m *Mongo) TransactionCreate(data map[string]interface{}) (string, error) {
	var err error
	for i := 0; i < 5; i++ {
		var id = bson.NewObjectId()
		data["_id"] = id
		err = m.c().Insert(data)
		if err == nil {
			return id.Hex(), nil
		}
	}
	return "", err
}

func (m *Mongo) TransactionUpdate(selector map[string]interface{}, update map[string]interface{}) error {
	var result []interface{}
	guidToID(&selector)
	//guidToID(&update)
	query := m.c().Find(selector)
	err := query.All(&result)
	if err != nil {
		return fmt.Errorf("qsuery result \"%+v\"", err.Error())
	}
	for _, val := range result {
		mval := val.(bson.M)
		for ukey, uval := range update {
			mval[ukey] = uval
		}
		id := mval["_id"]
		delete(mval, "id")
		err = m.c().UpdateId(id, mval)
		if err != nil {
			return fmt.Errorf("update by Id result \"%+v\"", err.Error())
		}
	}
	return err
}

func (m *Mongo) TransactionDelete(selector map[string]interface{}) error {
	guidToID(&selector)
	_, err := m.c().RemoveAll(selector)
	return err
}
