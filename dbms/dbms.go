package dbms

import (
	"fmt"
)

type Params map[string]interface{}

type Transactor interface {
	TransactionSelect(d map[string]interface{}) ([]map[string]interface{}, error)
	TransactionCreate(data map[string]interface{}) (string, error)
	TransactionUpdate(selector map[string]interface{}, update map[string]interface{}) error
	TransactionDelete(selector map[string]interface{}) error
}

type DB struct {
	Project  string
	Model    string
	Address  string
	DBMS     string
	Login    string
	Password string
}

func (d *DB) Create(data map[string]interface{}) (string, error) {
	var database Transactor
	var err error
	switch d.DBMS {
	case "mongo":
		database, err = NewMongo(d.Address, d.Project, d.Model)
		if err != nil {
			return "", err
		}
	default:
		return "", fmt.Errorf("Non-existent type of driver '%s'", d.DBMS)

	}
	return database.TransactionCreate(data)
}

func (d *DB) Select(data map[string]interface{}) ([]map[string]interface{}, error) {
	var database Transactor
	var err error
	switch d.DBMS {
	case "mongo":
		database, err = NewMongo(d.Address, d.Project, d.Model)
		if err != nil {
			return nil, err
		}
	default:
		return nil, fmt.Errorf("Non-existent type of driver '%s'", d.DBMS)

	}
	return database.TransactionSelect(data)
}

func (d *DB) Update(sel map[string]interface{}, upd map[string]interface{}) error {
	var database Transactor
	var err error
	switch d.DBMS {
	case "mongo":
		database, err = NewMongo(d.Address, d.Project, d.Model)
		if err != nil {
			return err
		}
	default:
		return fmt.Errorf("Non-existent type of driver '%s'", d.DBMS)

	}
	return database.TransactionUpdate(sel, upd)
}

func (d *DB) Delete(data map[string]interface{}) error {
	var database Transactor
	var err error
	switch d.DBMS {
	case "mongo":
		database, err = NewMongo(d.Address, d.Project, d.Model)
		if err != nil {
			return err
		}
	default:
		return fmt.Errorf("Non-existent type of driver '%s'", d.DBMS)

	}
	return database.TransactionDelete(data)
}
