package dbms

import (
	"testing"
)

var url = "localhost"
var db = "test"
var coll = "testColl"

var create = map[string]interface{}{
	"val1": "value1",
	"val2": 3.31,
	"val3": 123,
}

var update = map[string]interface{}{
	"val3": 235,
}

var result = map[string]interface{}{
	"val1": "value1",
	"val2": 3.31,
	"val3": 235,
}

func TestTransactor(t *testing.T) {
	var mongo, err = NewMongo(url, db, coll)
	if err != nil {
		t.Fatalf("Mongo init error: %v", err)
	}
	mongo.c().DropCollection()
	testCreate(mongo, t)
	testUpdate(mongo, t)
	testSelect(mongo, t)
	testDelete(mongo, t)

}
func testCreate(mongo Transactor, t *testing.T) {
	guid, err := mongo.TransactionCreate(create)
	if guid == "" {
		t.Fatalf("guid is empty: %v", guid)
	}
	if err != nil {
		t.Fatalf("Create error: %v", err)
	}
}

func testUpdate(mongo Transactor, t *testing.T) {
	err := mongo.TransactionUpdate(create, update)
	if err != nil {
		t.Fatalf("Update error: %v", err)
	}
}

func testSelect(mongo Transactor, t *testing.T) {
	res, err := mongo.TransactionSelect(result)
	if err != nil {
		t.Fatalf("Select error: %v", err)
	}
	if len(res) != 1 {
		t.Fatalf("Select result size bad, need len 1, get %v", len(res))
	}
}

func testDelete(mongo Transactor, t *testing.T) {
	err := mongo.TransactionDelete(result)
	res, err := mongo.TransactionSelect(result)
	if err != nil {
		t.Fatalf("Select error: %v", err)
	}

	if len(res) != 0 {
		t.Fatalf("Select result size bad, need len 0, get %v", len(res))
	}
}

func TestUpdateByGUID(t *testing.T) {
	var mongo, err = NewMongo(url, db, coll)
	if err != nil {
		t.Fatalf("Mongo init error: %v", err)
	}
	var params = make(map[string]interface{})
	guid, err := mongo.TransactionCreate(create)
	if err != nil {
		t.Fatalf("Create error: %v", err)
	}
	params["guid"] = guid
	mongo.TransactionUpdate(params, update)
	res, err := mongo.TransactionSelect(params)
	if err != nil {
		t.Fatalf("Select error: %v", err)
	}
	if len(res) != 1 {
		t.Fatalf("Select result size bad, need len 1, get %v", len(res))
	}
}

func TestDeleteByGUID(t *testing.T) {
	var mongo, err = NewMongo(url, db, coll)
	if err != nil {
		t.Fatalf("Mongo init error: %v", err)
	}
	var params = make(map[string]interface{})
	guid, err := mongo.TransactionCreate(create)
	if err != nil {
		t.Fatalf("Create error: %v", err)
	}
	params["guid"] = guid
	mongo.TransactionDelete(params)
	res, err := mongo.TransactionSelect(params)
	if err != nil {
		t.Fatalf("Select error: %v", err)
	}
	if len(res) != 0 {
		t.Fatalf("Select result size bad, need len 0, get %v", len(res))
	}
}
