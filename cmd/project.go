package cmd

import (
	"Clerk/root"
	"fmt"
	"io/ioutil"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

func addProject(c *cobra.Command, args []string) {
	name := args[0]
	filePath := args[1]

	b, err := ioutil.ReadFile(filePath) // just pass the file name
	if err != nil {
		fmt.Println(err)
	}
	code := string(b)

	address := viper.GetString("server.address")
	if []rune(address)[0] == ':' {
		address = "http://127.0.0.1" + address
	}
	err = root.CallNewProject(address, name, code)
	if err != nil {
		fmt.Println(err)
	}
}

func newProject(c *cobra.Command, args []string) {
	name := args[0]
	filePath := args[1]

	b, err := ioutil.ReadFile(filePath) // just pass the file name
	if err != nil {
		fmt.Print(err)
	}

	code := string(b)

	address := viper.GetString("server.address")

	err = root.CallNewProject(address, name, code)
}
