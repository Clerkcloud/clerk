package cmd

import (
	"Clerk/server"
	"fmt"
	"log"

	"github.com/kardianos/service"
	"github.com/spf13/cobra"
)

var logger service.Logger

type program struct {
	exit chan struct{}
}

func (p *program) Start(s service.Service) error {
	if service.Interactive() {
		logger.Info("Running in terminal.")
	} else {
		logger.Info("Running under service manager.")
	}
	p.exit = make(chan struct{})

	// Start should not block. Do the actual work async.
	go p.run()
	return nil
}

func (p *program) run() error {
	logger.Infof("Server running %v.", service.Platform())
	server.Start()
	return nil
}

func (p *program) Stop(s service.Service) error {
	// Any work in Stop should be quick, usually a few seconds at most.
	logger.Info("Server Stopping!")
	server.Stop()
	return nil
}

func Server(c *cobra.Command, args []string) {

	svcConfig := &service.Config{
		Name:        "Clerk",
		DisplayName: "Clerk server",
		Description: "BaaS system",
	}

	prg := &program{}
	s, err := service.New(prg, svcConfig)
	if err != nil {
		c.Println(err)
		return
	}

	errs := make(chan error, 5)
	logger, err = s.Logger(errs)
	if err != nil {
		c.Println(err)
		return
	}

	go func() {
		for {
			err := <-errs
			if err != nil {
				log.Println(err)
			}
		}
	}()

	if len(args) != 0 {
		switch args[0] {
		case "start":
			s.Install()
			err = s.Start()
		case "stop":
			err = s.Stop()
		case "restart":
			err = s.Restart()
		case "console":
			err = s.Run()
		default:
			err = fmt.Errorf("Undefined command: %s", args[0])
		}
		if err != nil {
			c.Printf("Failed to %s: %v", args[0], err)
		}
	} else {
		if err != nil {
			c.Printf("Bad count of args: requires at least one arg")
		}
	}

	return
}

/*
err = s.Run()
if err != nil {
	c.Println(err)
	return
}*/
