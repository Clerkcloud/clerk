package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "Clerk",
	Short: "Clerk is your personal Backend cloud.",
	Long: `Clerk is your personal Backend cloud.
More information on clerk.cloud`,
	Run: func(cmd *cobra.Command, args []string) {
		// Do Stuff Here
	},
}

func Execute() {
	rootCmd.AddCommand(serverCmd)
	rootCmd.AddCommand(addProjectCmd)
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "Interface for control Clerk server",
	Long: `
Interface for control Clerk server in background mode.
Use: server <command>
The following commands for managing the server state:
	start   - start server in background mode.
	stop    - stop server in background mode.
	restart - restart server in background mode.
	console - start server at console.
`,
	Args:       cobra.ExactArgs(1),
	ValidArgs:  []string{"start", "stop", "restart", "console"},
	ArgAliases: []string{"start", "stop", "restart", "console"},
	Run:        Server,
}

var addProjectCmd = &cobra.Command{
	Use:   "add",
	Short: "Add new project to server",
	Long: `	Add new project with code to server.
	Use: add <project name> <path>
	<project name>	- unique project name for server
	<path>		- path to the repository with the project code.
`,
	Args: cobra.ExactArgs(2),
	Run:  addProject,
}
