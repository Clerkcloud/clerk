package lexer

import (
	"Clerk/token"
	"testing"
)

func TestNextToken(t *testing.T) {
	input := `
Post model{
fields:
	body string!
	likes int
configs:
	routes = {
		resource = "a".func(a, b)
	}
	getMaxRaws = 5.go
	ok = 1.1
	val = [1, 2]
}

//users/{minClass:[0-9]+}
`
	tests := []struct {
		expectedType    token.TokenType
		expectedLiteral string
	}{
		{token.IDENT, "Post"},
		{token.IDENT, "model"},
		{token.LBRACE, "{"},
		{token.IDENT, "fields"},
		{token.COLON, ":"},
		{token.IDENT, "body"},
		{token.STRING_TYPE, "string"},
		{token.BANG, "!"},
		{token.IDENT, "likes"},
		{token.INT_TYPE, "int"},
		{token.IDENT, "configs"},
		{token.COLON, ":"},
		{token.IDENT, "routes"},
		{token.ASSIGN, "="},
		{token.LBRACE, "{"},
		{token.IDENT, "resource"},
		{token.ASSIGN, "="},
		{token.STRING, "a"},
		{token.DOT, "."},
		{token.IDENT, "func"},
		{token.LPAREN, "("},
		{token.IDENT, "a"},
		{token.COMMA, ","},
		{token.IDENT, "b"},
		{token.RPAREN, ")"},
		{token.RBRACE, "}"},
		{token.IDENT, "getMaxRaws"},
		{token.ASSIGN, "="},
		{token.INT, "5"},
		{token.DOT, "."},
		{token.IDENT, "go"},
		{token.IDENT, "ok"},
		{token.ASSIGN, "="},
		{token.FLOAT, "1.1"},
		{token.IDENT, "val"},
		{token.ASSIGN, "="},
		{token.LBRACKET, "["},
		{token.INT, "1"},
		{token.COMMA, ","},
		{token.INT, "2"},
		{token.RBRACKET, "]"},
		{token.RBRACE, "}"},
		{token.URI, "//users/{minClass:[0-9]+}"},
		{token.EOF, ""},
	}

	l := New(input)

	for i, tt := range tests {
		tok := l.NextToken()

		if tok.Type != tt.expectedType {
			t.Fatalf("tests[%v] - tokentype wrong. expected=%v, got=%v", i, tt.expectedType.ToString(), tok.Literal)
		}

		if tok.Literal != tt.expectedLiteral {
			t.Fatalf("tests[%v] - literal wrong. expected=%v, got=%v", i, tt.expectedLiteral, tok.Literal)
		}
	}

}
