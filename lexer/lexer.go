package lexer

//TODO: lexer не должен нигде возвращать ошибки
import (
	"Clerk/token"
	"fmt"
)

type Lexer struct {
	input        []rune
	position     int  // current position in input (points to current char)
	readPosition int  // current reading position in input (after current char)
	ch           rune // current char under examination
}

//New init lexer struct
func New(input string) *Lexer {
	l := &Lexer{input: []rune(input)}
	l.readChar()
	return l
}

func (l *Lexer) readChar() {
	if l.readPosition >= len(l.input) {
		l.ch = 0
	} else {
		l.ch = l.input[l.readPosition]
	}

	l.position = l.readPosition
	l.readPosition++
}

func (l *Lexer) isLastSymb(r rune) bool {
	if l.position == 0 {
		return false
	}
	if l.input[l.position] == r {
		return true
	}
	return false
}

//NextToken read next token from lexer struct
//Исправить присутствие ошибок
func (l *Lexer) NextToken() token.Token {
	var tok token.Token

	l.skipWhitespace()

	switch l.ch {
	case '+':
		tok = newToken(token.PLUS, l.ch)
	case '-':
		tok = newToken(token.MINUS, l.ch)
	case '*':
		tok = newToken(token.ASTERISK, l.ch)
	case '/':
		if l.peekChar() == '/' {
			tok = l.parseURI()
		} else {
			tok = newToken(token.SLASH, l.ch)
		}
	case '<':
		tok = newToken(token.LT, l.ch)
	case '>':
		tok = newToken(token.GT, l.ch)
	case '!':
		if l.peekChar() == '=' {
			ch := l.ch
			l.readChar()
			tok = token.Token{Type: token.NOT_EQ, Literal: string(ch) + string(l.ch)}
		} else {
			tok = newToken(token.BANG, l.ch)
		}
	case '.':
		tok = newToken(token.DOT, l.ch)
	case ',':
		tok = newToken(token.COMMA, l.ch)
	case '(':
		tok = newToken(token.LPAREN, l.ch)
	case ')':
		tok = newToken(token.RPAREN, l.ch)
	case ':':
		tok = newToken(token.COLON, l.ch)
	case '=':
		if l.peekChar() == '=' {
			ch := l.ch
			l.readChar()
			tok = token.Token{Type: token.EQ, Literal: string(ch) + string(l.ch)}
		} else {
			tok = newToken(token.ASSIGN, l.ch)
		}
	case '[':
		tok = newToken(token.LBRACKET, l.ch)
	case ']':
		tok = newToken(token.RBRACKET, l.ch)
	case '{':
		tok = newToken(token.LBRACE, l.ch)
	case '}':
		tok = newToken(token.RBRACE, l.ch)
	case '"':
		tok.Type = token.STRING
		var err error
		tok.Literal, err = l.readString()
		if err != nil {
			return tok
		}
	case 0:
		tok.Literal = ""
		tok.Type = token.EOF
	default:
		if isStartLetter(l.ch) {
			tok.Literal = l.readIdentifier()
			tok.Type = token.LookupIdent(tok.Literal)
			return tok
		} else if isDigit(l.ch) {
			tokType, val, err := l.readNumber()
			if err != nil {
				return tok
			}
			tok.Type = tokType
			tok.Literal = val
			return tok
		} else {
			tok = newToken(token.ILLEGAL, l.ch)
		}
	}
	l.readChar()
	return tok
}

//peekChar get current read position char
func (l *Lexer) peekChar() rune {
	if l.readPosition >= len(l.input) {
		return 0
	}

	return l.input[l.readPosition]
}

//readIdentifier get indentifier name
func (l *Lexer) readIdentifier() string {
	pos := l.position
	for isLetter(l.ch) {
		l.readChar()
	}

	return string(l.input[pos:l.position])
}

//readNumber looking int or float and identifies it
func (l *Lexer) readNumber() (token.TokenType, string, error) {
	position := l.position
	var tok token.TokenType
	var str string
	if l.ch == '0' {
		l.readChar()
		if isDigit(l.ch) {
			return tok, str, fmt.Errorf("Position %v: First zero numeric is not valid", l.readPosition)
		}
	}
	for isDigit(l.ch) {
		l.readChar()
	}

	isFloat := false

	if l.ch == '.' {
		if !isDigit(l.peekChar()) {
			str = string(l.input[position:l.position])
			return token.INT, str, nil
		}
		l.readChar()
		for isDigit(l.ch) {
			l.readChar()
		}
		isFloat = true
	}
	if l.ch == 'e' || l.ch == 'E' {
		l.readChar()
		if l.ch == '+' || l.ch == '-' {
			l.readChar()
			if !isDigit(l.ch) {
				return tok, str, fmt.Errorf("Position %v: the float number can not end with 'e-' or 'e+'", l.readPosition)
			}
		}
		if isDigit(l.ch) {
			l.readChar()
			for isDigit(l.ch) {
				l.readChar()
			}
		} else {
			return tok, str, fmt.Errorf("Position %v: the float number can not end with 'e'", l.readPosition)
		}
		isFloat = true
	}
	if isFloat {
		tok = token.FLOAT
	} else {
		tok = token.INT
	}
	str = string(l.input[position:l.position])
	return tok, str, nil
}

func (l *Lexer) skipWhitespace() {
	for l.isWhitespace() {
		l.readChar()
	}
}

func (l *Lexer) isWhitespace() bool {
	if l.ch == ' ' || l.ch == '\t' || l.ch == '\n' || l.ch == '\r' {
		return true
	}
	return false
}

func (l *Lexer) parseURI() token.Token {
	var str string
	for !l.isWhitespace() {
		str += string(l.ch)
		l.readChar()
		if l.peekChar() == 0 {
			break
		}
	}
	return token.Token{
		Type:    token.URI,
		Literal: str,
	}
}

func (l *Lexer) readString() (string, error) {
	str := ""
	for {
		l.readChar()
		if l.ch == '\\' {
			l.readChar()
			switch l.ch {
			case '"':
				str += "\""
				continue
			case '\\':
				str += "\\"
				continue
			case 'b':
				str += "\b"
				continue
			case 'f':
				str += "\f"
				continue
			case 'n':
				str += "\n"
				continue
			case 'r':
				str += "\r"
				continue
			case 't':
				str += "\t"
				continue
			case 0:
				return "", fmt.Errorf("Position %v: the closing character \" was not found", l.readPosition)
			default:
				str += "\\" + string(l.ch)
				continue
			}
		}
		if l.ch == '"' {
			break
		}
		if l.ch == 0 {
			return "", fmt.Errorf("Position %v: the closing character \" was not found", l.readPosition)
		}
		str += string(l.ch)
	}
	return str, nil
}

func (l *Lexer) isDots() bool {
	l.readChar()
	if l.ch != '.' {
		return false
	}
	l.readChar()
	if l.ch != '.' {
		return false
	}
	return true
}

func (l *Lexer) skipComment() {
	for {
		l.readChar()
		if l.ch == '\n' || l.ch == '\r' {
			return
		}
	}
}

func newToken(tokenType token.TokenType, ch rune) token.Token {
	return token.Token{
		Type:    tokenType,
		Literal: string(ch),
	}
}

func isDigit(ch rune) bool {
	return '0' <= ch && ch <= '9'
}

func isStartLetter(ch rune) bool {
	return 'a' <= ch && ch <= 'z' || 'A' <= ch && ch <= 'Z' || ch == '_'
}

func isLetter(ch rune) bool {
	return 'a' <= ch && ch <= 'z' || 'A' <= ch && ch <= 'Z' || ch == '_' || '0' <= ch && ch <= '9'
}
