package entity

import (
	"Clerk/ast"
	"fmt"
)

type Config interface {
	Set(string, ast.Expression) error
	Get(string) (ast.Expression, error)
	GetEmbedConfig(string) Config
	String() string
}

type StrConf struct {
	MaxLen   ast.Expression
	MinLen   ast.Expression
	DefValue ast.Expression
}

type IntConf struct {
	MaxValue ast.Expression
	MinValue ast.Expression
	DefValue ast.Expression
}

type FloatConf struct {
	MaxValue ast.Expression
	MinValue ast.Expression
	DefValue ast.Expression
}

type BoolConf struct {
	DefValue ast.Expression
}

type ModelConf struct {
	Database  Database
	DefString StrConf
	DefInt    IntConf
	DefFloat  FloatConf
	DefBool   BoolConf
}

type Database struct {
	Driver   ast.Expression
	Address  ast.Expression
	Login    ast.Expression
	Password ast.Expression
}

type CRUDStatement struct {
	Create ast.Expression
	Read   ast.Expression
	Update ast.Expression
	Delete ast.Expression
}

func NewStrConf() *StrConf {
	return &StrConf{}
}

func NewIntConf() *IntConf {
	return &IntConf{}
}

func NewBoolConf() *BoolConf {
	return &BoolConf{}
}

func NewFloatConf() *FloatConf {
	return &FloatConf{}
}

func NewModelConf() *ModelConf {
	return &ModelConf{}
}

/*
type EntityConf struct {
	//database
	//routes
	Access CRUDStatement
}

type RoleConf struct {
	Rights CRUDStatement
}
type DatabaseConf struct{

}
*/

func (s *StrConf) Set(conf string, val ast.Expression) error {
	switch conf {
	case "maxLen":
		s.MaxLen = val
		return nil
	case "minLen":
		s.MinLen = val
		return nil
	case "defValue":
		s.DefValue = val
		return nil
	default:
		return fmt.Errorf("Undefined name of config for string field: %s", conf)
	}
}

func (s *StrConf) Get(conf string) (ast.Expression, error) {
	switch conf {
	case "maxLen":
		return s.MaxLen, nil
	case "minLen":
		return s.MinLen, nil
	case "defValue":
		return s.DefValue, nil
	default:
		return nil, fmt.Errorf("Undefined name of config for string field: %s", conf)
	}
}

func (s *StrConf) GetEmbedConfig(name string) Config {
	return nil
}

func (s *StrConf) String() string {
	var res string
	if s.MaxLen != nil {
		res = res + "maxLen: " + s.MaxLen.String() + "\n"
	}
	if s.MinLen != nil {
		res = res + "minLen: " + s.MinLen.String() + "\n"
	}
	if s.DefValue != nil {
		res = res + "defValue: " + s.DefValue.String() + "\n"
	}
	return res
}

func (i *IntConf) Set(conf string, val ast.Expression) error {
	switch conf {
	case "maxValue":
		i.MaxValue = val
		return nil
	case "minValue":
		i.MinValue = val
		return nil
	case "defValue":
		i.DefValue = val
		return nil
	default:
		return fmt.Errorf("Undefined name of config for int field: %s", conf)
	}
}

func (i *IntConf) Get(conf string) (ast.Expression, error) {
	switch conf {
	case "maxValue":
		return i.MaxValue, nil
	case "minValue":
		return i.MinValue, nil
	case "defValue":
		return i.DefValue, nil
	default:
		return nil, fmt.Errorf("Undefined name of config for int field: %s", conf)
	}
}

func (i *IntConf) GetEmbedConfig(conf string) Config {
	return nil
}

func (i *IntConf) String() string {
	var res string
	if i.MaxValue != nil {
		res = res + "maxValue: " + i.MaxValue.String() + "\n"
	}
	if i.MinValue != nil {
		res = res + "minValue: " + i.MinValue.String() + "\n"
	}
	if i.DefValue != nil {
		res = res + "defValue: " + i.DefValue.String() + "\n"
	}
	return res
}

func (i *FloatConf) Set(conf string, val ast.Expression) error {
	switch conf {
	case "maxValue":
		i.MaxValue = val
		return nil
	case "minValue":
		i.MinValue = val
		return nil
	case "defValue":
		i.DefValue = val
		return nil
	default:
		return fmt.Errorf("Undefined name of config for float field: %s", conf)
	}
}

func (i *FloatConf) Get(conf string) (ast.Expression, error) {
	switch conf {
	case "maxValue":
		return i.MaxValue, nil
	case "minValue":
		return i.MinValue, nil
	case "defValue":
		return i.DefValue, nil
	default:
		return nil, fmt.Errorf("Undefined name of config for float field: %s", conf)
	}
}

func (s *FloatConf) GetEmbedConfig(name string) Config {
	return nil
}

func (f *FloatConf) String() string {
	var res string
	if f.MaxValue != nil {
		res = res + "maxLen: " + f.MaxValue.String() + "\n"
	}
	if f.MinValue != nil {
		res = res + "minLen: " + f.MinValue.String() + "\n"
	}
	if f.DefValue != nil {
		res = res + "defValue: " + f.DefValue.String() + "\n"
	}
	return res
}

func (i *BoolConf) Set(conf string, val ast.Expression) error {
	switch conf {
	case "defValue":
		i.DefValue = val
		return nil
	default:
		return fmt.Errorf("Undefined name of config for bool field: %s", conf)
	}
}

func (i *BoolConf) Get(conf string) (ast.Expression, error) {
	switch conf {
	case "defValue":
		return i.DefValue, nil
	default:
		return nil, fmt.Errorf("Undefined name of config for bool field: %s", conf)
	}
}

func (s *BoolConf) GetEmbedConfig(name string) Config {
	return nil
}

func (b *BoolConf) String() string {
	var res string
	if b.DefValue != nil {
		res = res + "defValue: " + b.DefValue.String() + "\n"
	}
	return res
}

func (m *ModelConf) Set(conf string, val ast.Expression) error {
	switch conf {
	default:
		return fmt.Errorf("Undefined name of config for model field: %s", conf)
	}
}

func (m *ModelConf) Get(conf string) (ast.Expression, error) {
	switch conf {
	default:
		return nil, fmt.Errorf("Undefined name of config for model field: %s", conf)
	}
}

func (m *ModelConf) GetEmbedConfig(name string) Config {
	switch name {
	case "string":
		return &m.DefString
	case "int":
		return &m.DefInt
	case "float":
		return &m.DefFloat
	case "bool":
		return &m.DefBool
	case "database":
		return &m.Database
	default:
		return nil
	}
}

func (m *ModelConf) String() string {
	var res string
	if m.Database.Login != nil {
		res = res + "database.login: " + m.Database.Login.String() + "\n"
	}
	if m.Database.Password != nil {
		res = res + "database.password: " + m.Database.Password.String() + "\n"
	}
	if m.Database.Driver != nil {
		res = res + "database.driver: " + m.Database.Driver.String() + "\n"
	}
	if m.Database.Address != nil {
		res = res + "database.address: " + m.Database.Address.String() + "\n"
	}
	if m.DefString.MaxLen != nil {
		res = res + "string.maxLen: " + m.DefString.MaxLen.String() + "\n"
	}
	if m.DefString.MinLen != nil {
		res = res + "string.minLen: " + m.DefString.MinLen.String() + "\n"
	}
	if m.DefString.DefValue != nil {
		res = res + "string.defValue: " + m.DefString.DefValue.String() + "\n"
	}
	if m.DefInt.MaxValue != nil {
		res = res + "int.maxValue: " + m.DefInt.MaxValue.String() + "\n"
	}
	if m.DefInt.MinValue != nil {
		res = res + "int.minValue: " + m.DefInt.MinValue.String() + "\n"
	}
	if m.DefInt.DefValue != nil {
		res = res + "int.defValue: " + m.DefInt.DefValue.String() + "\n"
	}
	if m.DefFloat.MaxValue != nil {
		res = res + "float.maxValue: " + m.DefFloat.MaxValue.String() + "\n"
	}
	if m.DefFloat.MinValue != nil {
		res = res + "float.minValue: " + m.DefFloat.MinValue.String() + "\n"
	}
	if m.DefFloat.DefValue != nil {
		res = res + "float.defValue: " + m.DefFloat.DefValue.String() + "\n"
	}
	if m.DefBool.DefValue != nil {
		res = res + "defValue: " + m.DefBool.DefValue.String() + "\n"
	}
	return res
}

func (d *Database) Set(conf string, val ast.Expression) error {
	switch conf {
	case "login":
		d.Login = val
	case "password":
		d.Password = val
	case "address":
		d.Address = val
	case "driver":
		d.Driver = val
	default:
		return fmt.Errorf("Undefined name of config for database field: %s", conf)
	}
	return nil
}

func (d *Database) Get(conf string) (ast.Expression, error) {
	switch conf {
	case "login":
		return d.Login, nil
	case "password":
		return d.Password, nil
	case "address":
		return d.Address, nil
	case "driver":
		return d.Driver, nil
	default:
		return nil, fmt.Errorf("Undefined name of config for database field: %s", conf)
	}
}

func (d *Database) GetEmbedConfig(name string) Config {
	return nil
}

func (d *Database) String() string {
	var res string
	if d.Login != nil {
		res = res + "login: " + d.Login.String() + "\n"
	}
	if d.Password != nil {
		res = res + "password: " + d.Password.String() + "\n"
	}
	if d.Driver != nil {
		res = res + "driver: " + d.Driver.String() + "\n"
	}
	if d.Address != nil {
		res = res + "Address: " + d.Address.String() + "\n"
	}
	return res
}
