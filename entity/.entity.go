package entity

import (
	"Clerk/dbms"
	"Clerk/object"
	"Clerk/token"
	"fmt"
	"strconv"

	"github.com/globalsign/mgo/bson"
)

type FieldType int

type Field struct {
	FieldType   token.Token
	NonNullable bool
	Configs     Config
}

type Crud interface {
	Create(createHash object.Hash) (string, error)
	Select(selectHash object.Hash) object.Hash //params: arr of guids
	Update(selectHash object.Hash, updateHash object.Hash) error
	Delete(selectHash object.Hash) error //params: arr of guids
}

type Model struct {
	Fields   map[string]Field
	Database dbms.Transactor
	Configs  Config
}

func NewModel() Model {
	return Model{
		Fields:  make(map[string]Field),
		Configs: NewModelConf(),
	}
}

//Сreate создаёт новый объект в БД и возвращает guid
func (m *Model) Create(createHash map[object.HashKey]object.HashPair) (string, error) {
	var createdHash = make(map[string]interface{})
	for _, v := range createHash {
		res, err := toInterface(v.Value)
		if err != nil {
			return "", err
		}
		strKey, ok := v.Key.(*object.String)
		if !ok {
			return "", fmt.Errorf("Bad type of key, need string, got %T", v.Key)
		}
		createdHash[strKey.Value] = res
	}
	m.checkFieldsForCreate(createdHash)
	return m.Database.TransactionCreate(createdHash)
}

//Select получает список GUIDОв и возврращает объекты из БД
func (m *Model) Select(selectHash map[object.HashKey]object.HashPair) ([]object.Hash, error) {
	var hashArr = []object.Hash{}
	var transactResult []map[string]interface{}
	var err error
	var sel = make(map[string]interface{})
	for _, item := range selectHash {
		val, err := toInterface(item.Value)
		if err != nil {
			return hashArr, err
		}
		strKey, ok := item.Key.(*object.String)
		if !ok {
			return hashArr, fmt.Errorf("Bad type of key, need string, got %T", item.Key)
		}
		sel[strKey.Value] = val
	}

	transactResult, err = m.Database.TransactionSelect(sel)
	if err != nil {
		return hashArr, err
	}

	for _, h := range transactResult {
		var hash = object.Hash{
			Pairs: map[object.HashKey]object.HashPair{},
		}
		for key, val := range h {
			objVal, err := toObject(val)
			if err != nil {
				return hashArr, err
			}
			objKey := object.String{
				Value: key,
			}
			hash.Pairs[objKey.HashKey()] = object.HashPair{
				Key:   &objKey,
				Value: objVal,
			}
		}
		hashArr = append(hashArr, hash)
	}
	return hashArr, nil
}

func (m *Model) Update(
	selectHash map[object.HashKey]object.HashPair,
	updateHash map[object.HashKey]object.HashPair,
) error {

	var sel = make(map[string]interface{})
	for _, item := range selectHash {
		val, err := toInterface(item.Value)
		if err != nil {
			return err
		}
		strKey, ok := item.Key.(*object.String)
		if !ok {
			return fmt.Errorf("Bad type of key, need string, got %T", item.Key)
		}
		sel[strKey.Value] = val
	}

	var upd = make(map[string]interface{})
	for _, item := range updateHash {
		val, err := toInterface(item.Value)
		if err != nil {
			return err
		}
		strKey, ok := item.Key.(*object.String)
		if !ok {
			return fmt.Errorf("Bad type of key, need string, got %T", item.Key)
		}
		upd[strKey.Value] = val
	}

	return m.Database.TransactionUpdate(sel, upd)
}

func (m *Model) Delete(selectHash map[object.HashKey]object.HashPair) error {
	var sel = make(map[string]interface{})
	for _, item := range selectHash {
		val, err := toInterface(item.Value)
		if err != nil {
			return err
		}
		strKey, ok := item.Key.(*object.String)
		if !ok {
			return fmt.Errorf("Bad type of key, need string, got %T", item.Key)
		}
		sel[strKey.Value] = val
	}

	return m.Database.TransactionDelete(sel)
}

func toInterface(o object.Object) (interface{}, error) {
	switch val := o.(type) {
	case *object.Integer:
		return val.Value, nil
	case *object.String:
		return val.Value, nil
	case *object.Boolean:
		return val.Value, nil
	case *object.Array:
		arr := make([]interface{}, len(val.Elements))
		for _, v := range val.Elements {
			res, err := toInterface(v)
			if err != nil {
				return nil, err
			}
			arr = append(arr, res)
		}
		return arr, nil
	default:
		return nil, fmt.Errorf("not supported type %T", val)
	}
}

func toObject(i interface{}) (object.Object, error) {
	switch val := i.(type) {
	case string:
		return &object.String{Value: val}, nil
	case bool:
		return &object.Boolean{Value: val}, nil
	case int:
		return &object.Integer{Value: int64(val)}, nil
	case int16:
		return &object.Integer{Value: int64(val)}, nil
	case int32:
		return &object.Integer{Value: int64(val)}, nil
	case int64:
		return &object.Integer{Value: val}, nil
	case []interface{}:
		var arr = object.Array{}
		for _, e := range val {
			obj, err := toObject(e)
			if err != nil {
				return nil, err
			}
			arr.Elements = append(arr.Elements, obj)
		}
	case map[interface{}]interface{}:
		var hash = object.Hash{}
		for key, val := range val {
			keyObj, err := toObject(key)
			if err != nil {
				return nil, err
			}
			valObj, err := toObject(val)
			if err != nil {
				return nil, err
			}
			hashKey, err := object.GetHashKey(keyObj)
			hash.Pairs[hashKey] = object.HashPair{
				Key:   keyObj,
				Value: valObj,
			}
		}
	case bson.ObjectId:
		return &object.String{Value: val.String()}, nil
	}
	return nil, fmt.Errorf("Undefined type of interface %T", i)
}

func (m *Model) checkFieldsForCreate(createdHash *map[string]interface{}) error {
	for mKey, mVal := range m.Fields {
		val, ok := (*createdHash)[mKey]
		if !ok && mVal.NonNullable {
			return fmt.Errorf("The value for the non nullable field %s is not set", key)
		}
		switch mVal.FieldType.Type {
		case token.STRING:
			conv, err := convertToString(val)
			if err != nil {
				return fmt.Errorf("Field %s: %s", mKey, err.Error())
			}

			maxLen, err := mVal.Configs.Get("maxLen")

		}
	}
	return nil
}

func convertToString(i interface{}) (string, error) {
	switch val := i.(type) {
	case string:
		return val, nil
	case bool:
		return strconv.FormatBool(val), nil
	case float32:
		return strconv.FormatFloat(float64(val), 'f', -1, 64), nil
	case float64:
		return strconv.FormatFloat(val, 'f', -1, 64), nil
	case int8:
		return strconv.FormatInt(int64(val), 10), nil
	case int16:
		return strconv.FormatInt(int64(val), 10), nil
	case int32:
		return strconv.FormatInt(int64(val), 10), nil
	case int64:
		return strconv.FormatInt(int64(val), 10), nil
	default:
		return "", fmt.Errorf("Can't convert type %T to string", i)
	}
}
