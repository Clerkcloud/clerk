package entity

import (
	"Clerk/ast"
	"Clerk/token"
)

type Project struct {
	Name   string
	Code   string
	Models map[string]Model
	Routes map[RouteID]Route
}

type FieldType int

type Field struct {
	Name        string
	FieldType   token.Token
	NonNullable bool
	Configs     Config
}

type Fielder interface {
	SetField(name string, field Field) bool
	GetField(name string) (*Field, bool)
}

type Model struct {
	Name    string
	Fields  []Field
	Configs Config
	Project string
}

type RouteID struct {
	URI    string
	Method string
}

type Route struct {
	URI    string
	Method string
	Fields []Field
	Action ast.Expression
}

func NewProject() Project {
	return Project{
		Models: make(map[string]Model),
		Routes: make(map[RouteID]Route),
	}
}
func NewModel(name string, project string) Model {
	var fields []Field
	fields = append(
		fields,
		Field{
			Name: "guid",
			FieldType: token.Token{
				Literal: "string",
				Type:    token.STRING_TYPE,
			},
			Configs: NewStrConf(),
		},
	)
	return Model{
		Configs: NewModelConf(),
		Project: project,
		Name:    name,
		Fields:  fields,
	}
}

func NewRoute(URI string, method string) Route {
	return Route{
		URI:    URI,
		Method: method,
	}
}

/*
//Сreate создаёт новый объект в БД и возвращает guid
func (m *Model) Create(createHash map[string]interface{}) (string, error) {
	return m.Database.TransactionCreate(createHash)
}

//Select получает список GUIDОв и возврращает объекты из БД
func (m *Model) Select(selectHash map[string]interface{}) ([]map[string]interface{}, error) {

	return m.Database.TransactionSelect(selectHash)
}

func (m *Model) Update(
	selectHash map[string]interface{},
	updateHash map[string]interface{},
) error {
	return m.Database.TransactionUpdate(selectHash, updateHash)
}

func (m *Model) Delete(selectHash map[string]interface{}) error {

	return m.Database.TransactionDelete(selectHash)
}
*/

func (m *Model) GetField(name string) (*Field, bool) {
	for _, f := range m.Fields {
		if f.Name == name {
			return &f, true
		}
	}
	return nil, false
}

func (m *Model) SetField(name string, field Field) bool {
	for _, f := range m.Fields {
		if f.Name == name {
			return false
		}
	}
	field.Name = name
	m.Fields = append(m.Fields, field)
	return true
}

func (m *Route) GetField(name string) (*Field, bool) {
	for _, f := range m.Fields {
		if f.Name == name {
			return &f, true
		}
	}
	return nil, false
}

func (m *Route) SetField(name string, field Field) bool {
	for _, f := range m.Fields {
		if f.Name == name {
			return false
		}
	}
	field.Name = name
	m.Fields = append(m.Fields, field)
	return true
}
