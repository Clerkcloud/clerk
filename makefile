# Go parameters
GOCMD=go
GOBUILD=$(GOCMD) build
BINARY_NAME=mybinary
BINARY_UNIX=$(BINARY_NAME)_unix

# Cross compilation
build-os:
	GOOS=linux GOARCH=amd64 $(GOBUILD) -o /home/zaur/Clerk/wiki/src/linux64/Clerk
	GOOS=linux GOARCH=386 $(GOBUILD) -o /home/zaur/Clerk/wiki/src/linux32/Clerk
	GOOS=windows GOARCH=amd64 $(GOBUILD) -o /home/zaur/Clerk/wiki/src/windows64/Clerk
	GOOS=windows GOARCH=386 $(GOBUILD) -o /home/zaur/Clerk/wiki/src/windows32/Clerk
	GOOS=darwin GOARCH=amd64 $(GOBUILD) -o /home/zaur/Clerk/wiki/src/macos64/Clerk
	GOOS=darwin GOARCH=386 $(GOBUILD) -o /home/zaur/Clerk/wiki/src/macos32/Clerk