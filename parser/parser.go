package parser

import (
	"Clerk/ast"
	"Clerk/entity"
	"Clerk/lexer"
	"Clerk/token"
	"fmt"
	"net/http"
	"strconv"
)

type (
	configFn      func()
	prefixParseFn func() ast.Expression
	infixParseFn  func(ast.Expression) ast.Expression
)

const (
	_           int = iota
	LOWEST          //lowest
	EQUALS          // ==
	LESSGREATER     // > or <
	SUM             // +
	PRODUCT         // *
	PREFIX          // -X or !X
	CALL            // myFunction(X)
	INDEX
)

var precedences = map[token.TokenType]int{
	token.EQ:       EQUALS,
	token.NOT_EQ:   EQUALS,
	token.LT:       LESSGREATER,
	token.GT:       LESSGREATER,
	token.PLUS:     SUM,
	token.MINUS:    SUM,
	token.SLASH:    PRODUCT,
	token.ASTERISK: PRODUCT,
	token.LPAREN:   CALL,
	token.DOT:      CALL,
	token.LBRACKET: INDEX,
}

type Parser struct {
	l      *lexer.Lexer
	errors []string

	curToken  token.Token
	peekToken token.Token

	project entity.Project

	configFns      map[string]configFn
	prefixParseFns map[token.TokenType]prefixParseFn
	infixParseFns  map[token.TokenType]infixParseFn
}

func New(l *lexer.Lexer) *Parser {
	p := &Parser{
		l:      l,
		errors: []string{},
	}

	p.nextToken()
	p.nextToken()

	p.prefixParseFns = make(map[token.TokenType]prefixParseFn)
	p.registerPrefix(token.IDENT, p.parseIdentifier)
	p.registerPrefix(token.INT, p.parseIntegerLiteral)
	p.registerPrefix(token.BANG, p.parsePrefixExpression)
	p.registerPrefix(token.MINUS, p.parsePrefixExpression)
	p.registerPrefix(token.TRUE, p.parseBoolean)
	p.registerPrefix(token.FALSE, p.parseBoolean)
	p.registerPrefix(token.LPAREN, p.parseGroupedExpression)
	p.registerPrefix(token.IF, p.parseIfExpression)
	p.registerPrefix(token.STRING, p.parseStringLiteral)
	p.registerPrefix(token.LBRACKET, p.parseArrayLiteral)
	p.registerPrefix(token.LBRACE, p.parseHashLiteral)

	p.infixParseFns = make(map[token.TokenType]infixParseFn)
	p.registerInfix(token.PLUS, p.parseInfixExpression)
	p.registerInfix(token.MINUS, p.parseInfixExpression)
	p.registerInfix(token.SLASH, p.parseInfixExpression)
	p.registerInfix(token.ASTERISK, p.parseInfixExpression)
	p.registerInfix(token.EQ, p.parseInfixExpression)
	p.registerInfix(token.NOT_EQ, p.parseInfixExpression)
	p.registerInfix(token.LT, p.parseInfixExpression)
	p.registerInfix(token.GT, p.parseInfixExpression)
	p.registerInfix(token.LPAREN, p.parseCallExpression)
	p.registerInfix(token.LBRACKET, p.parseIndexExpression)
	p.registerInfix(token.DOT, p.parseInfixExpression)
	return p
}

func (p *Parser) Errors() []string {
	return p.errors
}

func (p *Parser) nextToken() {
	p.curToken = p.peekToken
	p.peekToken = p.l.NextToken()
}

func (p *Parser) ParseProgram() entity.Project {
	p.project = entity.NewProject()
	for p.curToken.Type != token.EOF && len(p.errors) == 0 {
		switch {
		case p.curTokenIs(token.AT):
			p.nextToken()
			p.parseGlobalConfig()
		case p.curTokenIs(token.IDENT):
			if p.peekToken.Literal == "role" || p.peekToken.Literal == "model" {
				p.parseModel()
			} else {
				p.unexpectedError()
				p.nextToken()
				//TODO error
			}
		case p.curTokenIs(token.URI):
			p.parseRoute()
		default:
			p.unexpectedError()
			return p.project
		}
	}
	return p.project
}

func (p *Parser) ParseExpression() ast.Expression {
	return p.parseExpression(LOWEST)
}

func (p *Parser) parseModel() {
	var ok bool
	var mName = p.curToken.Literal
	//var mType = p.getToken().Literal
	_, ok = p.project.Models[mName]
	if ok {
		p.errors = append(
			p.errors,
			fmt.Sprintf("Duplicated name of model: %v", p.curToken),
		)
	}
	p.nextToken()
	if !p.expectPeek(token.LBRACE) {
		return
	}
	p.nextToken()

	var flag bool
	var model = entity.NewModel(mName, p.project.Name)

	for !p.curTokenIs(token.RBRACE) {
		if p.curTokenIs(token.EOF) {
			p.unexpectedError()
			return
		}
		if p.curToken.Literal == "configs" && p.peekTokenIs(token.COLON) {
			flag = false
			p.nextToken()
			p.nextToken()
		}
		if p.curToken.Literal == "fields" && p.peekTokenIs(token.COLON) {
			flag = true
			p.nextToken()
			p.nextToken()
		}
		if flag {
			p.parseFieldsBlock(&model.Fields)
		} else {
			p.parseConfigsBlock(model.Configs)
		}
	}
	p.nextToken()
	p.project.Models[mName] = model
}

func (p *Parser) parseRoute() {
	var ok bool
	var route = p.curToken.Literal[1:len(p.curToken.Literal)]

	p.nextToken()
	if !p.curTokenIs(token.IDENT) {
		p.expectPeek(token.IDENT)
		return
	}
	var method = p.curToken.Literal
	if method == "CRUD" {
		modelToken := p.peekToken
		p.nextToken()
		p.nextToken()
		if modelToken.Type != token.IDENT {
			p.expectPeek(token.IDENT)
			return
		}
		p.buildModelCRUD(modelToken.Literal, route)
		return
	}

	_, ok = p.project.Routes[entity.RouteID{
		URI:    route,
		Method: method,
	}]
	if ok {
		p.errors = append(
			p.errors,
			fmt.Sprintf("Duplicated route: %v", p.curToken),
		)
	}

	if !p.expectPeek(token.LBRACE) {
		return
	}
	p.nextToken()

	var URI = entity.NewRoute(route, method)
	p.parseFieldsBlock(&URI.Fields)
	if !p.curTokenIs(token.RBRACE) {
		p.curError(token.RBRACE)
		return
	}

	if !p.expectPeek(token.LPAREN) {
		return
	}

	URI.Action = p.ParseExpression()

	if !p.curTokenIs(token.RPAREN) {
		p.curError(token.RPAREN)
		return
	}
	p.nextToken()
	p.project.Routes[entity.RouteID{
		URI:    route,
		Method: method,
	}] = URI
}

func (p *Parser) buildModelCRUD(modelName string, route string) error {
	model, ok := p.project.Models[modelName]
	if !ok {
		return fmt.Errorf("Undefined model %s", modelName)
	}
	p.addCreateRouteForModel(model, route)
	p.addSelectRouteForModel(model, route)
	p.addUpdateRouteForModel(model, route)
	p.addDeleteRouteForModel(model, route)
	return nil
}

//addCreateRouteForModel - добавляет роут для создания модели
func (p *Parser) addCreateRouteForModel(model entity.Model, route string) {
	model.Fields = model.Fields[1:len(model.Fields)]
	exprStr := model.Name + ".create({"
	for i, field := range model.Fields {
		exprStr += `"` + field.Name + `":` + field.Name
		if (i + 1) != len(model.Fields) {
			exprStr += ","
		}
	}
	exprStr += " })"
	l := lexer.New(exprStr)
	pp := New(l)
	astExp := pp.parseExpression(1)

	p.project.Routes[entity.RouteID{
		URI:    route,
		Method: http.MethodPost,
	}] = entity.Route{
		URI:    route,
		Method: http.MethodPost,
		Fields: model.Fields,
		Action: astExp,
	}
}

//addSelectRouteForModel - добавляет роут для получения модели по GUID
func (p *Parser) addSelectRouteForModel(model entity.Model, route string) {
	exprStr := model.Name + `.select({"guid":guid})`
	l := lexer.New(exprStr)
	pp := New(l)
	astExp := pp.parseExpression(1)

	route += "/{guid}"
	method := http.MethodGet
	fields := model.Fields[:len(model.Fields)-1]

	p.project.Routes[entity.RouteID{
		URI:    route,
		Method: method,
	}] = entity.Route{
		URI:    route,
		Method: method,
		Fields: fields,
		Action: astExp,
	}
}

//addUpdateRouteForModel добавляет роут для обновления модели по GUID
func (p *Parser) addUpdateRouteForModel(model entity.Model, route string) {
	exprStr := model.Name + `.update({"guid":guid},{`
	updDatafields := model.Fields[1:len(model.Fields)]
	for i, field := range updDatafields {
		exprStr += `"` + field.Name + `":` + field.Name
		if (i + 1) != len(model.Fields) {
			exprStr += ","
		}
	}
	exprStr += "})"
	l := lexer.New(exprStr)
	pp := New(l)
	astExp := pp.parseExpression(1)
	route += "/{guid}"
	method := http.MethodPatch
	p.project.Routes[entity.RouteID{
		URI:    route,
		Method: method,
	}] = entity.Route{
		URI:    route,
		Method: method,
		Fields: model.Fields,
		Action: astExp,
	}
}

//addDeleteRouteForModel добавляет роут для удаления модели по GUID
func (p *Parser) addDeleteRouteForModel(model entity.Model, route string) {
	exprStr := model.Name + `.delete({"guid":guid})`
	l := lexer.New(exprStr)
	pp := New(l)
	astExp := pp.parseExpression(1)

	route += "/{guid}"
	method := http.MethodDelete
	fields := model.Fields[:len(model.Fields)-1]
	p.project.Routes[entity.RouteID{
		URI:    route,
		Method: method,
	}] = entity.Route{
		URI:    route,
		Method: method,
		Fields: fields,
		Action: astExp,
	}
}

func (p *Parser) parseFieldsBlock(fields *[]entity.Field) {
	for !p.curTokenIs(token.RBRACE) {
		if p.curToken.Literal == "configs" &&
			p.peekTokenIs(token.COLON) {
			return
		}
		if p.curTokenIs(token.EOF) {
			p.unexpectedError()
			return
		}
		if !p.curTokenIs(token.IDENT) {
			p.curError(token.IDENT)
			p.nextToken()
			continue
		}
		*fields = append(*fields, p.parseFieldType())
	}
}

func (p *Parser) parseFieldType() entity.Field {
	var field = entity.Field{
		Name: p.curToken.Literal,
	}
	p.nextToken()
	field.FieldType = p.curToken

	switch field.FieldType.Type {
	case token.STRING_TYPE:
		field.Configs = entity.NewStrConf()
	case token.INT_TYPE:
		field.Configs = entity.NewIntConf()
	case token.BOOL_TYPE:
		field.Configs = entity.NewBoolConf()
	case token.FLOAT_TYPE:
		field.Configs = entity.NewFloatConf()
	//case token.IDENT:
	default:
		p.unexpectedError()
		p.nextToken()
		return field
	}
	p.nextToken()
	if p.curTokenIs(token.BANG) {
		field.NonNullable = true
		p.nextToken()
	}
	if p.curTokenIs(token.LBRACE) {
		p.nextToken()
		p.parseConfigsBlock(field.Configs)
		p.nextToken()
	}
	return field
}

func (p *Parser) parseConfigsBlock(configs entity.Config) {
	for !p.curTokenIs(token.RBRACE) {
		if p.curToken.Literal == "fields" && p.peekTokenIs(token.COLON) {
			return
		}
		if p.curTokenIs(token.EOF) {
			return
		}

		var curConf = configs

		if !p.curTokenIs(token.IDENT) {
			p.curError(token.IDENT)
			p.nextToken()
			return
		}

		var paramName = p.curToken.Literal
		for p.peekTokenIs(token.DOT) {
			p.nextToken()
			p.nextToken()
			var c = curConf.GetEmbedConfig(paramName)
			if c == nil {
				p.errors = append(
					p.errors,
					fmt.Sprintf("Undefind embeded config %v for %T", paramName, curConf),
				)
				return
			}
			curConf = c
			paramName = p.curToken.Literal
		}

		if !p.expectPeek(token.COLON) {
			return
		}
		p.nextToken()

		curConf.Set(paramName, p.parseExpression(LOWEST))
		p.nextToken()
	}
}

func (p *Parser) parseGlobalConfig() {

}

/*
func (p *Parser) buildProgram() sop.Program {
	var program = sop.Program{}

	for eName, entity := range p.draft.Entities {
		if entity.Type.Literal == "model" {
			program.Models[eName] = p.buildModelAst(eName)
		} else if entity.Type.Literal == "role" {
			program.Roles[eName] = p.buildRoleAst(eName)
		}
	}
	return program
}

func (p *Parser) buildModelAst(eName string) sop.Model {
	return sop.Model{}
}

func (p *Parser) buildRoleAst(eName string) sop.Role {
	return sop.Role{}
}
*/

func (p *Parser) noPrefixParseFnError(t token.TokenType) {
	msg := fmt.Sprintf("no prefix parse function for %v found", t.ToString())
	p.errors = append(p.errors, msg)
}

func (p *Parser) parseExpression(precedence int) ast.Expression {
	prefix := p.prefixParseFns[p.curToken.Type]
	if prefix == nil {
		p.noPrefixParseFnError(p.curToken.Type)
		return nil
	}
	leftExp := prefix()
	for precedence < p.peekPrecedence() {
		infix := p.infixParseFns[p.peekToken.Type]
		if infix == nil {
			return leftExp
		}
		p.nextToken()
		leftExp = infix(leftExp)
	}
	return leftExp
}

func (p *Parser) peekPrecedence() int {
	if p, ok := precedences[p.peekToken.Type]; ok {
		return p
	}
	return LOWEST
}
func (p *Parser) curPrecedence() int {
	if p, ok := precedences[p.curToken.Type]; ok {
		return p
	}
	return LOWEST
}

func (p *Parser) parseIdentifier() ast.Expression {
	return &ast.Identifier{Token: p.curToken, Value: p.curToken.Literal}
}

func (p *Parser) parseNominal() ast.Expression {
	runeStr := []rune(p.curToken.Literal)
	subStr := string(runeStr[1:len(runeStr)])
	return &ast.Nominal{Token: p.curToken, Value: subStr}
}

func (p *Parser) parseIntegerLiteral() ast.Expression {
	lit := &ast.IntegerLiteral{Token: p.curToken}
	value, err := strconv.ParseInt(p.curToken.Literal, 0, 64)
	if err != nil {
		msg := fmt.Sprintf("could not parse %q as integer", p.curToken.Literal)
		p.errors = append(p.errors, msg)
		return nil
	}
	lit.Value = value
	return lit
}

func (p *Parser) parsePrefixExpression() ast.Expression {
	expression := &ast.PrefixExpression{
		Token:    p.curToken,
		Operator: p.curToken.Literal,
	}
	p.nextToken()
	expression.Right = p.parseExpression(PREFIX)
	return expression
}

func (p *Parser) parseInfixExpression(left ast.Expression) ast.Expression {
	expression := &ast.InfixExpression{
		Token:    p.curToken,
		Operator: p.curToken.Literal,
		Left:     left,
	}
	precedence := p.curPrecedence()
	p.nextToken()
	expression.Right = p.parseExpression(precedence)
	return expression
}

func (p *Parser) parseBoolean() ast.Expression {
	return &ast.Boolean{Token: p.curToken, Value: p.curTokenIs(token.TRUE)}
}

func (p *Parser) parseGroupedExpression() ast.Expression {
	p.nextToken()
	exp := p.parseExpression(LOWEST)
	if !p.expectPeek(token.RPAREN) {
		return nil
	}
	return exp
}

func (p *Parser) parseIfExpression() ast.Expression {
	expression := &ast.IfExpression{Token: p.curToken}
	if !p.expectPeek(token.LPAREN) {
		return nil
	}
	p.nextToken()
	expression.Condition = p.parseExpression(LOWEST)
	if !p.expectPeek(token.RPAREN) {
		return nil
	}
	if !p.expectPeek(token.LBRACE) {
		return nil
	}
	p.nextToken()
	expression.Consequence = p.parseExpression(LOWEST)
	if !p.expectPeek(token.RBRACE) {
		return nil
	}
	if p.peekTokenIs(token.ELSE) {
		p.nextToken()
		if !p.expectPeek(token.LBRACE) {
			return nil
		}
		p.nextToken()
		expression.Alternative = p.parseExpression(LOWEST)
	}
	return expression
}

func (p *Parser) parseStringLiteral() ast.Expression {
	return &ast.StringLiteral{Token: p.curToken, Value: p.curToken.Literal}
}

func (p *Parser) parseCallExpression(function ast.Expression) ast.Expression {
	exp := &ast.CallExpression{Token: p.curToken, Function: function}
	exp.Arguments = p.parseExpressionList(token.RPAREN)
	return exp
}

func (p *Parser) parseArrayLiteral() ast.Expression {
	array := &ast.ArrayLiteral{Token: p.curToken}
	array.Elements = p.parseExpressionList(token.RBRACKET)
	return array
}

func (p *Parser) parseExpressionList(end token.TokenType) []ast.Expression {
	list := []ast.Expression{}
	if p.peekTokenIs(end) {
		p.nextToken()
		return list
	}
	p.nextToken()
	list = append(list, p.parseExpression(LOWEST))
	for p.peekTokenIs(token.COMMA) {
		p.nextToken()
		p.nextToken()
		list = append(list, p.parseExpression(LOWEST))
	}
	if !p.expectPeek(end) {
		return nil
	}
	return list
}

func (p *Parser) parseIndexExpression(left ast.Expression) ast.Expression {
	exp := &ast.IndexExpression{Token: p.curToken, Left: left}
	p.nextToken()
	exp.Index = p.parseExpression(LOWEST)
	if !p.expectPeek(token.RBRACKET) {
		return nil
	}
	return exp
}

func (p *Parser) parseHashLiteral() ast.Expression {
	hash := &ast.HashLiteral{Token: p.curToken}
	hash.Pairs = make(map[ast.Expression]ast.Expression)
	for !p.peekTokenIs(token.RBRACE) {
		p.nextToken()
		key := p.parseExpression(LOWEST)
		if !p.expectPeek(token.COLON) {
			return nil
		}
		p.nextToken()
		value := p.parseExpression(LOWEST)
		hash.Pairs[key] = value
		if !p.peekTokenIs(token.RBRACE) && !p.expectPeek(token.COMMA) {
			return nil
		}
	}
	if !p.expectPeek(token.RBRACE) {
		return nil
	}
	return hash
}

func (p *Parser) curTokenIs(t token.TokenType) bool {
	return p.curToken.Type == t
}

func (p *Parser) peekTokenIs(t token.TokenType) bool {
	return p.peekToken.Type == t
}

func (p *Parser) registerConfigFn(str string, fn configFn) {
	p.configFns[str] = fn
}

func (p *Parser) getToken() token.Token {
	tok := p.curToken
	p.nextToken()
	return tok
}
func (p *Parser) expectPeek(t token.TokenType) bool {
	if p.peekTokenIs(t) {
		p.nextToken()
		return true
	} else {
		p.peekError(t)
		return false
	}
}

func (p *Parser) peekError(t token.TokenType) {
	msg := fmt.Sprintf("expected next token to be %v, got %v instead", t.ToString(), p.peekToken.Type.ToString())
	p.errors = append(p.errors, msg)
}

func (p *Parser) curError(t token.TokenType) {
	msg := fmt.Sprintf("expected current token to be %v, got %v (%v) instead", t.ToString(), p.curToken.Type.ToString(), p.curToken.Literal)
	p.errors = append(p.errors, msg)
}

func (p *Parser) unexpectedError() {
	msg := fmt.Sprintf("unexpected token %v", p.curToken.Type.ToString())
	p.errors = append(p.errors, msg)
}

func (p *Parser) registerPrefix(tokenType token.TokenType, fn prefixParseFn) {
	p.prefixParseFns[tokenType] = fn
}
func (p *Parser) registerInfix(tokenType token.TokenType, fn infixParseFn) {
	p.infixParseFns[tokenType] = fn
}
