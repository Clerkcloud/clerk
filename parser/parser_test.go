package parser

import (
	"Clerk/ast"
	"Clerk/entity"
	"Clerk/lexer"
	"fmt"
	"net/http"
	"testing"
)

func TestParseFieldsBLock(t *testing.T) {
	var input = `
	field string{
		minLen: a+b
		maxLen: 12
	}
	field2 int!	
	boolStr bool!{
		defValue: true
	}
	Float float{}
	configs:
	`
	var fields = map[string]struct {
		fType    string
		fConfigs string
		nonNull  bool
	}{
		"field": {
			fType:    "string type",
			fConfigs: "maxLen: 12\nminLen: (a + b)\n",
			nonNull:  false,
		},
		"field2": {
			fType:    "int type",
			fConfigs: "",
			nonNull:  true,
		},
		"boolStr": {
			fType:    "bool type",
			fConfigs: "defValue: true\n",
			nonNull:  true,
		},
		"Float": {
			fType:    "float type",
			fConfigs: "",
			nonNull:  false,
		},
	}

	l := lexer.New(input)
	p := New(l)
	var parsedFields []entity.Field
	p.parseFieldsBlock(&parsedFields)
	if len(p.errors) != 0 {
		t.Errorf("Incorrect count of errors, need 0, got %v, %v", len(p.errors), p.errors)
	}
	if p.curToken.Literal != "configs" {
		t.Fatalf("Bad result parsing field blocks, need cur literal: configs, get: %s", p.curToken.Literal)
	}

	for fName, fVal := range fields {
		var f entity.Field
		var ok bool
		for _, el := range parsedFields {
			if el.Name == fName {
				ok = true
				f = el
				break
			}
		}
		if !ok {
			t.Errorf("Parsed fields have not field %s", fName)
			continue
		}
		if f.FieldType.Type.ToString() != fVal.fType {
			t.Errorf("Parsed fields have bad field type, need %s, got %s",
				fVal.fType,
				f.FieldType.Type.ToString(),
			)
			continue
		}
		if f.Configs.String() != fVal.fConfigs {
			t.Errorf("Parsed fields have bad configs, need %s, got %s",
				fVal.fConfigs,
				f.Configs.String(),
			)
			continue
		}

		if f.NonNullable != fVal.nonNull {
			t.Errorf("Parsed fields have bad NonNull, need %s, got %s",
				fVal.nonNull,
				f.NonNullable,
			)
			continue
		}
	}
}

func TestParseModelConfigs(t *testing.T) {
	var input = `
	database.driver: "mongo"
	database.login: "hello"
	database.password: n+m
}
	`
	var output = "database.login: \"hello\"\ndatabase.password: (n + m)\ndatabase.driver: \"mongo\"\n"

	l := lexer.New(input)
	p := New(l)
	var config = entity.NewModelConf()
	p.parseConfigsBlock(config)

	if len(p.errors) != 0 {
		t.Errorf("Incorrect count of errors, need 0, got %v, %v", len(p.errors), p.errors)
	}

	if config.String() != output {
		t.Errorf("Parsed fields have bad configs, need %s, got %s",
			output,
			config.String(),
		)
	}
}

func TestModelParse(t *testing.T) {
	var tCases = []struct {
		input  string
		models []string
	}{
		{
			input: `
			Person model{
				fields: 
				Hopa string
				configs:
			}

			Files model{
			}`,
			models: []string{"Person", "Files"},
		},
		{
			input: `
		Person model{
			fields: 
			configs:
		}`,
			models: []string{"Person"},
		},
		{
			input: `
		Person model{}`,
			models: []string{"Person"},
		},
	}

	for i, tCase := range tCases {
		l := lexer.New(tCase.input)
		p := New(l)
		project := p.ParseProgram()
		if len(p.errors) != 0 {
			t.Fatalf("Case %v. Errors %v", i, p.errors)
		}
		for _, tName := range tCase.models {
			_, ok := project.Models[tName]
			if !ok {
				t.Errorf("Case %v. Parsed models have not model %s", i, tName)
			}
		}
	}
}

func TestParseRoute(t *testing.T) {
	var input = `
	//route/{param} GET {
		field1 string	
		field2 string
	}(
		1+1
	)
	`

	l := lexer.New(input)
	p := New(l)
	project := p.ParseProgram()
	if len(p.Errors()) != 0 {
		t.Fatalf("Errors: %v", p.Errors())
	}

	route, ok := project.Routes[entity.RouteID{
		URI:    "/route/{param}",
		Method: "GET",
	}]
	if !ok {
		t.Errorf("Route don't parsed")
	}

	if route.Method != "GET" {
		t.Errorf("Bad method type, expected GET, got %s", route.Method)
	}
	if len(route.Fields) != 2 {
		t.Errorf("Bad count of fields, expected = 2, got = %v", len(route.Fields))
	}
}

func TestCrudRoute(t *testing.T) {
	var input = `
	Crud model{
	fields:
		field string!
	}
	
	//route CRUD Crud
	`
	l := lexer.New(input)
	p := New(l)
	project := p.ParseProgram()
	if len(p.Errors()) != 0 {
		t.Fatalf("Errors: %v", p.Errors())
	}
	testCreateCrudRoute(t, project.Routes)
	testSelectCrudRoute(t, project.Routes)
	testUpdateCrudRoute(t, project.Routes)
	testDeleteCrudRoute(t, project.Routes)
}

func testCreateCrudRoute(t *testing.T, routes map[entity.RouteID]entity.Route) {
	route, ok := routes[entity.RouteID{
		URI:    "/route",
		Method: http.MethodPost,
	}]
	if !ok {
		t.Errorf("Missing create method for CRUD")
		return
	}
	if len(route.Fields) != 1 {
		t.Errorf("CRUD create test: expected len of fields = 1, got = %v\n %v", len(route.Fields), route.Fields)
		return
	}
	if route.Fields[0].Name != "field" ||
		route.Fields[0].FieldType.Literal != "string" {
		t.Errorf(" expected name = field, type = string, got name = %s, type = %s",
			route.Fields[0].Name,
			route.Fields[0].FieldType.Literal)
	}

	expectedAct := `(Crud . create)({"field":field})`
	if route.Action.String() != expectedAct {
		t.Errorf("Bad action, expected =\n%s\ngot=\n%s", expectedAct, route.Action.String())
	}
}

func testSelectCrudRoute(t *testing.T, routes map[entity.RouteID]entity.Route) {
	route, ok := routes[entity.RouteID{
		URI:    "/route/{guid}",
		Method: http.MethodGet,
	}]
	if !ok {
		t.Errorf("Missing create method for CRUD")
		return
	}
	if len(route.Fields) != 1 {
		t.Errorf("CRUD select test: expected len of fields = 1, got = %v\n %v", len(route.Fields), route.Fields)
		return
	}
	if route.Fields[0].Name != "guid" ||
		route.Fields[0].FieldType.Literal != "string" {
		t.Errorf(" expected name = guid, type = string, got name = %s, type = %s",
			route.Fields[0].Name,
			route.Fields[0].FieldType.Literal)
	}

	expectedAct := `(Crud . select)({"guid":guid})`
	if route.Action.String() != expectedAct {
		t.Errorf("Bad action, expected =\n%s\ngot=\n%s", expectedAct, route.Action.String())
	}
}

func testUpdateCrudRoute(t *testing.T, routes map[entity.RouteID]entity.Route) {
	route, ok := routes[entity.RouteID{
		URI:    "/route/{guid}",
		Method: http.MethodPatch,
	}]
	if !ok {
		t.Errorf("Missing create method for CRUD")
		return
	}
	if len(route.Fields) != 2 {
		t.Errorf("CRUD update test: expected len of fields = 2, got = %v\n %v", len(route.Fields), route.Fields)
		return
	}
	if route.Fields[0].Name != "guid" ||
		route.Fields[0].FieldType.Literal != "string" {
		t.Errorf(" expected name = guid, type = string, got name = %s, type = %s",
			route.Fields[0].Name,
			route.Fields[0].FieldType.Literal)
	}

	if route.Fields[1].Name != "field" ||
		route.Fields[1].FieldType.Literal != "string" {
		t.Errorf(" expected name = field, type = string, got name = %s, type = %s",
			route.Fields[0].Name,
			route.Fields[0].FieldType.Literal)
	}

	expectedAct := `(Crud . update)({"guid":guid}, {"field":field})`
	if route.Action.String() != expectedAct {
		t.Errorf("Bad action, expected =\n%s\ngot=\n%s", expectedAct, route.Action.String())
	}
}

func testDeleteCrudRoute(t *testing.T, routes map[entity.RouteID]entity.Route) {
	route, ok := routes[entity.RouteID{
		URI:    "/route/{guid}",
		Method: http.MethodDelete,
	}]
	if !ok {
		t.Errorf("Missing create method for CRUD")
		return
	}
	if len(route.Fields) != 1 {
		t.Errorf("CRUD delete test: expected len of fields = 1, got = %v\n %v", len(route.Fields), route.Fields)
		return
	}
	if route.Fields[0].Name != "guid" ||
		route.Fields[0].FieldType.Literal != "string" {
		t.Errorf(" expected name = guid, type = string, got name = %s, type = %s",
			route.Fields[0].Name,
			route.Fields[0].FieldType.Literal)
	}

	expectedAct := `(Crud . delete)({"guid":guid})`
	if route.Action.String() != expectedAct {
		t.Errorf("Bad action, expected =\n%s\ngot=\n%s", expectedAct, route.Action.String())
	}
}

func testIdentifier(t *testing.T, exp ast.Expression, value string) bool {
	ident, ok := exp.(*ast.Identifier)
	if !ok {
		t.Errorf("exp not *ast.Identifier. got=%T", exp)
		return false
	}
	if ident.Value != value {
		t.Errorf("ident.Value not %s. got=%s", value, ident.Value)
		return false
	}
	if ident.TokenLiteral() != value {
		t.Errorf("ident.TokenLiteral not %s. got=%s", value,
			ident.TokenLiteral())
		return false
	}
	return true
}

func testLiteralExpression(
	t *testing.T,
	exp ast.Expression,
	expected interface{},
) bool {
	switch v := expected.(type) {
	case int:
		return testIntegerLiteral(t, exp, int64(v))
	case int64:
		return testIntegerLiteral(t, exp, v)
	case string:
		return testIdentifier(t, exp, v)
	case bool:
		return testBooleanLiteral(t, exp, v)
	}
	t.Errorf("type of exp not handled. got=%T", exp)
	return false
}

func testInfixExpression(t *testing.T, exp ast.Expression, left interface{},
	operator string, right interface{}) bool {
	opExp, ok := exp.(*ast.InfixExpression)
	if !ok {
		t.Errorf("exp is not ast.OperatorExpression. got=%T(%s)", exp, exp)
		return false
	}
	if !testLiteralExpression(t, opExp.Left, left) {
		return false
	}
	if opExp.Operator != operator {
		t.Errorf("exp.Operator is not '%s'. got=%q", operator, opExp.Operator)
		return false
	}
	if !testLiteralExpression(t, opExp.Right, right) {
		return false
	}
	return true
}

func testBooleanLiteral(t *testing.T, exp ast.Expression, value bool) bool {
	bo, ok := exp.(*ast.Boolean)
	if !ok {
		t.Errorf("exp not *ast.Boolean. got=%T", exp)
		return false
	}
	if bo.Value != value {
		t.Errorf("bo.Value not %t. got=%t", value, bo.Value)
		return false
	}
	if bo.TokenLiteral() != fmt.Sprintf("%t", value) {
		t.Errorf("bo.TokenLiteral not %t. got=%s",
			value, bo.TokenLiteral())
		return false
	}
	return true
}

func TestIdentifierExpression(t *testing.T) {
	input := "foobar"
	l := lexer.New(input)
	p := New(l)
	exp := p.parseExpression(LOWEST)
	checkParserErrors(t, p, "TestIdentifierExpression")
	ident, ok := exp.(*ast.Identifier)
	if !ok {
		t.Fatalf("exp not *ast.Identifier. got=%T", exp)
	}
	if ident.Value != "foobar" {
		t.Errorf("ident.Value not %s. got=%s", "foobar", ident.Value)
	}
	if ident.TokenLiteral() != "foobar" {
		t.Errorf("ident.TokenLiteral not %s. got=%s", "foobar",
			ident.TokenLiteral())
	}
}

/*
func TestNominalExpression(t *testing.T) {
	input := ":foobar"
	l := lexer.New(input)
	p := New(l)
	exp := p.parseExpression(LOWEST)
	checkParserErrors(t, p, "TestNominalExpression")
	nominal, ok := exp.(*ast.Nominal)
	if !ok {
		t.Fatalf("exp not *ast.Nominal. got=%T", exp)
	}
	if nominal.Value != "foobar" {
		t.Errorf("nominal.Value not %s. got=%s", "foobar", nominal.Value)
	}
	if nominal.TokenLiteral() != ":foobar" {
		t.Errorf("ident.TokenLiteral not %s. got=%s", "foobar",
			nominal.TokenLiteral())
	}
}*/

func TestIntegerLiteralExpression(t *testing.T) {
	input := "5"
	l := lexer.New(input)
	p := New(l)
	exp := p.parseExpression(LOWEST)
	checkParserErrors(t, p, "TestIntegerLiteralExpression")
	literal, ok := exp.(*ast.IntegerLiteral)
	if !ok {
		t.Fatalf("exp not *ast.IntegerLiteral. got=%T", exp)
	}
	if literal.Value != 5 {
		t.Errorf("literal.Value not %d. got=%d", 5, literal.Value)
	}
	if literal.TokenLiteral() != "5" {
		t.Errorf("literal.TokenLiteral not %s. got=%s", "5",
			literal.TokenLiteral())
	}
}

func TestParsingPrefixExpressions(t *testing.T) {
	prefixTests := []struct {
		input    string
		operator string
		value    interface{}
	}{
		{"!5;", "!", 5},
		{"-15;", "-", 15},
		{"!true;", "!", true},
		{"!false;", "!", false},
	}
	for _, tt := range prefixTests {
		l := lexer.New(tt.input)
		p := New(l)
		checkParserErrors(t, p, "TestParsingPrefixExpressions")
		expAst := p.parseExpression(LOWEST)
		exp, ok := expAst.(*ast.PrefixExpression)
		if !ok {
			t.Fatalf("program.Statements[0] is not ast.ExpressionStatement. got=%T", expAst)
		}

		if exp.Operator != tt.operator {
			t.Fatalf("exp.Operator is not '%s'. got=%s", tt.operator, exp.Operator)
		}

		if !testLiteralExpression(t, exp.Right, tt.value) {
			return
		}
	}
}

func testIntegerLiteral(t *testing.T, il ast.Expression, value int64) bool {
	integ, ok := il.(*ast.IntegerLiteral)
	if !ok {
		t.Errorf("il not *ast.IntegerLiteral. got=%T", il)
		return false
	}
	if integ.Value != value {
		t.Errorf("integ.Value not %d. got=%d", value, integ.Value)
		return false
	}
	if integ.TokenLiteral() != fmt.Sprintf("%d", value) {
		t.Errorf("integ.TokenLiteral not %d. got=%s", value,
			integ.TokenLiteral())
		return false
	}
	return true
}

func TestParsingInfixExpressions(t *testing.T) {
	infixTests := []struct {
		input      string
		leftValue  interface{}
		operator   string
		rightValue interface{}
	}{
		{"5 + 5", 5, "+", 5},
		{"5 - 5", 5, "-", 5},
		{"5 * 5", 5, "*", 5},
		{"5 / 5", 5, "/", 5},
		{"5 > 5", 5, ">", 5},
		{"5 < 5", 5, "<", 5},
		{"5 == 5", 5, "==", 5},
		{"5 != 5", 5, "!=", 5},
		{"true == true", true, "==", true},
		{"true != false", true, "!=", false},
		{"false == false", false, "==", false},
	}
	for _, tt := range infixTests {
		l := lexer.New(tt.input)
		p := New(l)
		expAst := p.parseExpression(LOWEST)
		checkParserErrors(t, p, "TestParsingInfixExpressions")
		exp, ok := expAst.(*ast.InfixExpression)
		if !ok {
			t.Fatalf("exp is not ast.InfixExpression. got=%T", expAst)
		}
		if !testLiteralExpression(t, exp.Left, tt.leftValue) {
			t.Errorf("Expression literal is not %v, got=%T ", tt.leftValue, exp.Left)
		}
		if !testLiteralExpression(t, exp.Right, tt.rightValue) {
			t.Errorf("Expression literal is not %v, got=%T ", tt.rightValue, exp.Right)
		}
	}
}

func TestOperatorPrecedenceParsing(t *testing.T) {
	tests := []struct {
		input    string
		expected string
	}{
		{
			"-a * b",
			"((-a) * b)",
		},
		{
			"!-a",
			"(!(-a))",
		},
		{
			"a + b + c",
			"((a + b) + c)",
		},
		{
			"a + b - c",
			"((a + b) - c)",
		},
		{
			"a * b * c",
			"((a * b) * c)",
		},
		{
			"a * b / c",
			"((a * b) / c)",
		},
		{
			"a + b / c",
			"(a + (b / c))",
		},
		{
			"true",
			"true",
		},
		{
			"false",
			"false",
		},
		{
			"3 > 5 == false",
			"((3 > 5) == false)",
		},
		{
			"3 < 5 == true",
			"((3 < 5) == true)",
		},
		{
			"a + b * c + d / e - f",
			"(((a + (b * c)) + (d / e)) - f)",
		},
		{
			"5 > 4 == 3 < 4",
			"((5 > 4) == (3 < 4))",
		},
		{
			"5 < 4 != 3 > 4",
			"((5 < 4) != (3 > 4))",
		},
		{
			"3 + 4 * 5 == 3 * 1 + 4 * 5",
			"((3 + (4 * 5)) == ((3 * 1) + (4 * 5)))",
		},
		{
			"3 + 4 * 5 == 3 * 1 + 4 * 5",
			"((3 + (4 * 5)) == ((3 * 1) + (4 * 5)))",
		},
		{
			"1 + (2 + 3) + 4",
			"((1 + (2 + 3)) + 4)",
		},
		{
			"(5 + 5) * 2",
			"((5 + 5) * 2)",
		},
		{
			"2 / (5 + 5)",
			"(2 / (5 + 5))",
		},
		{
			"-(5 + 5)",
			"(-(5 + 5))",
		},
		{
			"!(true == true)",
			"(!(true == true))",
		},
		{
			"a + add(b * c) + d",
			"((a + add((b * c))) + d)",
		},
		{
			"add(a, b, 1, 2 * 3, 4 + 5, add(6, 7 * 8))",
			"add(a, b, 1, (2 * 3), (4 + 5), add(6, (7 * 8)))",
		},
		{
			"add(a + b + c * d / f + g)",
			"add((((a + b) + ((c * d) / f)) + g))",
		},
		{
			"a * [1, 2, 3, 4][b * c] * d",
			"((a * ([1, 2, 3, 4][(b * c)])) * d)",
		},
		{
			"add(a * b[2], b[1], 2 * [1, 2][1])",
			"add((a * (b[2])), (b[1]), (2 * ([1, 2][1])))",
		},
	}
	for _, tt := range tests {
		l := lexer.New(tt.input)
		p := New(l)
		program := p.parseExpression(LOWEST)
		checkParserErrors(t, p, "TestOperatorPrecedenceParsing")
		actual := program.String()
		if actual != tt.expected {
			t.Errorf("expected=%q, got=%q", tt.expected, actual)
		}
	}
}

func TestIfExpression(t *testing.T) {
	input := `if (x < y) { 3+2 }else{5}`
	l := lexer.New(input)
	p := New(l)
	expression := p.parseExpression(LOWEST)
	checkParserErrors(t, p, "TestIfExpression")
	exp, ok := expression.(*ast.IfExpression)
	if !ok {
		t.Fatalf("stmt.Expression is not ast.IfExpression. got=%T",
			expression)
	}
	if !testInfixExpression(t, exp.Condition, "x", "<", "y") {
		t.Errorf("'if' expression bad condition parse")
	}

	consequence := exp.Consequence
	_, ok = consequence.(*ast.InfixExpression)
	if !ok {
		t.Fatalf("'Consequence' is not ast.InfixExpression. got=%T",
			consequence)
	}

	alternative := exp.Alternative
	_, ok = alternative.(*ast.IntegerLiteral)
	if !ok {
		t.Fatalf("'Alternative' is not ast.IntegerLiteral. got=%T",
			alternative)
	}

}

func TestStringLiteralExpression(t *testing.T) {
	input := `"hello world"`
	l := lexer.New(input)
	p := New(l)
	expr := p.parseExpression(LOWEST)
	checkParserErrors(t, p, "TestStringLiteralExpression")
	literal, ok := expr.(*ast.StringLiteral)
	if !ok {
		t.Fatalf("exp not *ast.StringLiteral. got=%T", expr)
	}
	if literal.Value != "hello world" {
		t.Errorf("literal.Value not %q. got=%q", "hello world", literal.Value)
	}
}

func checkParserErrors(t *testing.T, p *Parser, callFucntionName string) {
	errors := p.Errors()
	if len(errors) == 0 {
		return
	}

	t.Errorf("%s: parser has %d errors", callFucntionName, len(errors))
	for _, msg := range errors {
		t.Errorf("%s: parser error: %q", callFucntionName, msg)
	}
	t.FailNow()
}

func TestCallExpressionParsing(t *testing.T) {
	input := "add(1, 2 * 3, 4 + 5)"
	l := lexer.New(input)
	p := New(l)
	astExp := p.parseExpression(1)
	checkParserErrors(t, p, "TestCallExpressionParsing")
	exp, ok := astExp.(*ast.CallExpression)
	if !ok {
		t.Fatalf("astExp is not ast.CallExpression. got=%T",
			astExp)
	}
	if !testIdentifier(t, exp.Function, "add") {
		return
	}
	if len(exp.Arguments) != 3 {
		t.Fatalf("wrong length of arguments. got=%d", len(exp.Arguments))
	}
	testLiteralExpression(t, exp.Arguments[0], 1)
	testInfixExpression(t, exp.Arguments[1], 2, "*", 3)
	testInfixExpression(t, exp.Arguments[2], 4, "+", 5)
}

func TestMethodExpressionParsing(t *testing.T) {
	input := "\"root\".len(1, 2 * 3, 4 + 5)"
	l := lexer.New(input)
	p := New(l)
	astExp := p.parseExpression(1)
	checkParserErrors(t, p, "TestMethodExpressionParsing")
	exp, ok := astExp.(*ast.CallExpression)
	if !ok {
		t.Fatalf("astExp is not ast.CallExpression. got=%T",
			astExp)
	}

	funcExp, ok := exp.Function.(*ast.InfixExpression)

	if !ok {
		t.Fatalf("funcExp is not ast.InfixExpression. got=%T",
			exp)
	}

	if funcExp.Operator != "." {
		t.Fatalf("wrong operator, need \".\" got=%v", funcExp.Operator)
	}

	if !testIdentifier(t, funcExp.Right, "len") {
		return
	}

	left, ok := funcExp.Left.(*ast.StringLiteral)

	if !ok {
		t.Fatalf("Left funcExp is not string literal, got: %T", funcExp.Left)
	}

	if left.Value != "root" {
		t.Errorf("funcExp left value is not 'root', got: %s", left.Value)
	}

	if len(exp.Arguments) != 3 {
		t.Fatalf("wrong length of arguments. got=%d", len(exp.Arguments))
	}
	testLiteralExpression(t, exp.Arguments[0], 1)
	testInfixExpression(t, exp.Arguments[1], 2, "*", 3)
	testInfixExpression(t, exp.Arguments[2], 4, "+", 5)
}

func TestParsingArrayLiterals(t *testing.T) {
	input := "[1, 2 * 2, 3 + 3]"
	l := lexer.New(input)
	p := New(l)
	program := p.ParseExpression()
	checkParserErrors(t, p, "TestParsingArrayLiterals")
	array, ok := program.(*ast.ArrayLiteral)
	if !ok {
		t.Fatalf("exp not ast.ArrayLiteral. got=%T", program)
	}
	if len(array.Elements) != 3 {
		t.Fatalf("len(array.Elements) not 3. got=%d", len(array.Elements))
	}
	testIntegerLiteral(t, array.Elements[0], 1)
	testInfixExpression(t, array.Elements[1], 2, "*", 2)
	testInfixExpression(t, array.Elements[2], 3, "+", 3)
}

func TestParsingIndexExpressions(t *testing.T) {
	input := "myArray[1 + 1]"
	l := lexer.New(input)
	p := New(l)
	program := p.ParseExpression()
	checkParserErrors(t, p, "TestParsingIndexExpressions")
	indexExp, ok := program.(*ast.IndexExpression)
	if !ok {
		t.Fatalf("exp not *ast.IndexExpression. got=%T", program)
	}
	if !testIdentifier(t, indexExp.Left, "myArray") {
		return
	}
	if !testInfixExpression(t, indexExp.Index, 1, "+", 1) {
		return
	}
}

func TestParsingHashLiteralsStringKeys(t *testing.T) {
	input := `{"one": 1, "two": 2, "three": 3}`
	l := lexer.New(input)
	p := New(l)
	program := p.ParseExpression()
	checkParserErrors(t, p, "TestParsingHashLiteralsStringKeys")
	hash, ok := program.(*ast.HashLiteral)
	if !ok {
		t.Fatalf("exp is not ast.HashLiteral. got=%T", program)
	}
	if len(hash.Pairs) != 3 {
		t.Errorf("hash.Pairs has wrong length. got=%d", len(hash.Pairs))
	}
	expected := map[string]int64{
		"one":   1,
		"two":   2,
		"three": 3,
	}
	for key, value := range hash.Pairs {
		literal, ok := key.(*ast.StringLiteral)
		if !ok {
			t.Errorf("key is not ast.StringLiteral. got=%T", key)
		}
		expectedValue := expected[literal.Token.Literal]
		testIntegerLiteral(t, value, expectedValue)
	}
}

func TestParsingEmptyHashLiteral(t *testing.T) {
	input := "{}"
	l := lexer.New(input)
	p := New(l)
	program := p.ParseExpression()
	checkParserErrors(t, p, "TestParsingEmptyHashLiteral")
	hash, ok := program.(*ast.HashLiteral)
	if !ok {
		t.Fatalf("exp is not ast.HashLiteral. got=%T", program)
	}
	if len(hash.Pairs) != 0 {
		t.Errorf("hash.Pairs has wrong length. got=%d", len(hash.Pairs))
	}
}

func TestParsingHashLiteralsWithExpressions(t *testing.T) {
	input := `{"one": 0 + 1, "two": 10 - 8, "three": 15 / 5}`
	l := lexer.New(input)
	p := New(l)
	program := p.ParseExpression()
	checkParserErrors(t, p, "TestParsingHashLiteralsWithExpressions")
	hash, ok := program.(*ast.HashLiteral)
	if !ok {
		t.Fatalf("exp is not ast.HashLiteral. got=%T", program)
	}
	if len(hash.Pairs) != 3 {
		t.Errorf("hash.Pairs has wrong length. got=%d", len(hash.Pairs))
	}
	tests := map[string]func(ast.Expression){
		"one": func(e ast.Expression) {
			testInfixExpression(t, e, 0, "+", 1)
		},
		"two": func(e ast.Expression) {
			testInfixExpression(t, e, 10, "-", 8)
		},
		"three": func(e ast.Expression) {
			testInfixExpression(t, e, 15, "/", 5)
		},
	}
	for key, value := range hash.Pairs {
		literal, ok := key.(*ast.StringLiteral)
		if !ok {
			t.Errorf("key is not ast.StringLiteral. got=%T", key)
			continue
		}
		testFunc, ok := tests[literal.Token.Literal]
		if !ok {
			t.Errorf("No test function for key %q found", literal.String())
			continue
		}
		testFunc(value)
	}
}
