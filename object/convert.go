package object

import (
	"fmt"
	"strconv"
)

func ConvToString(o Object) (string, error) {
	switch obj := o.(type) {
	case *String:
		return obj.Value, nil
	case *Boolean:
		return strconv.FormatBool(obj.Value), nil
	case *Integer:
		return strconv.FormatInt(obj.Value, 10), nil
	default:
		return "", fmt.Errorf("Can't convert type %T to string", obj)
	}
}

func ConvToInt(o Object) (int64, error) {
	switch obj := o.(type) {
	case *String:
		return strconv.ParseInt(obj.Value, 10, 64)
	case *Boolean:
		bitSetVar := int64(0)
		if obj.Value {
			bitSetVar = 1
		}
		return bitSetVar, nil
	case *Integer:
		return obj.Value, nil
	default:
		return 0, fmt.Errorf("Can't convert type %T to string", obj)
	}
}

func ConvToBool(o Object) (bool, error) {
	switch obj := o.(type) {
	case *String:
		if obj.Value == "true" {
			return true, nil
		} else if obj.Value == "false" {
			return false, nil
		} else {
			return false, fmt.Errorf("Can't convert '%s' to Boolean", obj.Value)
		}
	case *Boolean:
		return obj.Value, nil
	case *Integer:
		if obj.Value == 1 {
			return true, nil
		} else if obj.Value == 0 {
			return false, nil
		} else {
			return false, fmt.Errorf("Can't convert '%v' to Boolean", obj.Value)
		}
	default:
		return false, fmt.Errorf("Can't convert type %t to Boolean", obj)
	}
}
func GetHashKey(o Object) (HashKey, error) {
	switch obj := o.(type) {
	case *Integer:
		return obj.HashKey(), nil
	case *String:
		return obj.HashKey(), nil
	case *Boolean:
		return obj.HashKey(), nil
	default:
		return HashKey{}, fmt.Errorf("Not hashable type %v", obj.Type())
	}
}

func ConvertToObject(val interface{}) (Object, error) {
	switch conv := val.(type) {
	case int64:
		return &Integer{
			Value: conv,
		}, nil
	case string:
		return &String{
			Value: conv,
		}, nil
	case bool:
		return &Boolean{
			Value: conv,
		}, nil
	case []interface{}:
		var arr Array
		for _, val := range conv {
			cVal, err := ConvertToObject(val)
			if err != nil {
				return nil, err
			}
			arr.Elements = append(arr.Elements, cVal)
		}
		return &arr, nil
	case map[string]interface{}:
		var hash Hash
		hash.Pairs = make(map[HashKey]HashPair)
		for key, val := range conv {
			cKey := String{
				Value: key,
			}
			cVal, err := ConvertToObject(val)
			if err != nil {
				return nil, err
			}
			hashKey, err := GetHashKey(&cKey)
			if err != nil {
				return nil, err
			}
			hash.Pairs[hashKey] = HashPair{
				Key:   &cKey,
				Value: cVal,
			}
		}
		return &hash, nil

	case []map[string]interface{}:
		var arr Array
		for _, elem := range conv {
			var hash Hash
			hash.Pairs = make(map[HashKey]HashPair)
			for key, val := range elem {
				cKey := String{
					Value: key,
				}
				cVal, err := ConvertToObject(val)
				if err != nil {
					return nil, err
				}
				hashKey, err := GetHashKey(&cKey)
				if err != nil {
					return nil, err
				}
				hash.Pairs[hashKey] = HashPair{
					Key:   &cKey,
					Value: cVal,
				}
			}
			arr.Elements = append(arr.Elements, &hash)
		}
		return &arr, nil
	case error:
		return &Error{
			Message: conv.Error(),
		}, nil
	case nil:
		return &Null{}, nil
	default:
		return nil, fmt.Errorf("Undefined type for convertint to hash key %T", val)
	}
}
