package object

import (
	"Clerk/entity"
	"bytes"
	"fmt"
	"hash/fnv"
	"strings"
)

type ObjectType string

const (
	INTEGER_OBJ = "INTEGER"
	BOOLEAN_OBJ = "BOOLEAN"
	NULL_OBJ    = "NULL"
	STRING_OBJ  = "STRING"
	NOMINAL_OBJ = "NOMINAL"
	NOMPAIR_OBJ = "NOMPAIR"
	ERROR_OBJ   = "ERROR"
	BUILTIN_OBJ = "BUILTIN"
	METHOD_OBJ  = "METHOD"
	ARRAY_OBJ   = "ARRAY"
	HASH_OBJ    = "HASH"
	MODEL_OBJ   = "MODEL"
)

type Object interface {
	Type() ObjectType
	Inspect() string
}

type Integer struct {
	Value int64
}

type String struct {
	Value string
}

type Boolean struct {
	Value bool
}

type Nominal struct {
	Value string
}

type NomPair struct {
	Nominal    Nominal
	Operator   string
	AnotherVal Object
}

type Array struct {
	Elements []Object
}

type Hashable interface {
	HashKey() HashKey
}

type Hash struct {
	Pairs map[HashKey]HashPair
}

type HashKey struct {
	Type  ObjectType
	Value uint64
}

type HashPair struct {
	Key   Object
	Value Object
}

type Model struct {
	Ref entity.Model
}

type Error struct {
	Message string
}

type Builtin struct {
	Fn BuiltinFunction
}

type BuiltinFunction func(args ...Object) Object

func (i *Integer) Type() ObjectType { return INTEGER_OBJ }
func (i *Integer) Inspect() string  { return fmt.Sprintf("%d", i.Value) }

func (b *Boolean) Type() ObjectType { return BOOLEAN_OBJ }
func (b *Boolean) Inspect() string  { return fmt.Sprintf("%t", b.Value) }

type Null struct{}

func (n *Null) Type() ObjectType { return NULL_OBJ }
func (n *Null) Inspect() string  { return "null" }

func (s *String) Type() ObjectType { return STRING_OBJ }
func (s *String) Inspect() string  { return "\"" + s.Value + "\"" }

func (e *Error) Type() ObjectType { return ERROR_OBJ }
func (e *Error) Inspect() string  { return "ERROR: " + e.Message }

func (n *Nominal) Type() ObjectType { return NOMINAL_OBJ }
func (n *Nominal) Inspect() string  { return ":" + n.Value }

func (n *NomPair) Type() ObjectType { return NOMPAIR_OBJ }
func (n *NomPair) Inspect() string {
	return n.Nominal.Inspect() +
		" " +
		n.Operator +
		" " +
		n.AnotherVal.Inspect()
}
func (b *Builtin) Type() ObjectType { return BUILTIN_OBJ }
func (b *Builtin) Inspect() string  { return "builtin function" }

func (ao *Array) Type() ObjectType { return ARRAY_OBJ }

func (ao *Array) Inspect() string {
	var out bytes.Buffer
	elements := []string{}
	for _, e := range ao.Elements {
		elements = append(elements, e.Inspect())
	}
	out.WriteString("[")
	out.WriteString(strings.Join(elements, ", "))
	out.WriteString("]")
	return out.String()
}

func (b *Boolean) HashKey() HashKey {
	var value uint64
	if b.Value {
		value = 1
	} else {
		value = 0
	}
	return HashKey{Type: b.Type(), Value: value}
}

func (i *Integer) HashKey() HashKey {
	return HashKey{Type: i.Type(), Value: uint64(i.Value)}
}

func (s *String) HashKey() HashKey {
	h := fnv.New64a()
	h.Write([]byte(s.Value))
	return HashKey{Type: s.Type(), Value: h.Sum64()}
}

func (h *Hash) Type() ObjectType { return HASH_OBJ }
func (h *Hash) Inspect() string {
	var out bytes.Buffer
	pairs := []string{}
	for _, pair := range h.Pairs {
		pairs = append(pairs, fmt.Sprintf("%s: %s",
			pair.Key.Inspect(), pair.Value.Inspect()))
	}
	out.WriteString("{")
	out.WriteString(strings.Join(pairs, ", "))
	out.WriteString("}")
	return out.String()
}

func (m *Model) Type() ObjectType { return MODEL_OBJ }
func (m *Model) Inspect() string {
	return ""
	/* TODO
	return m.String()
	*/
}
