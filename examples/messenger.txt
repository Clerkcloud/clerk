User role{
fields:
    username String!{
        auth = true
    }
    name String
    surname String
	blackList [User]
	whiteList [User]
    image Image!{
        minWidth = 300
        minHeight = 400
        maxHeight = 1200
        maxWidth = 1400
        formats = ["gif", "png", "jpg"]
        thumbs = [
            {
                name = "avatar-min"
                size = "128x128#"
            },
            {
                name = "avatar-max"
                size = "256x256#"
            }
        ]
	}
params:
    access = [
		Chat: {
			read   = (this isInclude .members)
			create = true
			update = (this isInclude .moders)
			delete = (this isInclude .moders)
		},
		Message: {
			read   = (.chat == NULL && (this == .creator || .to == creator)) ||
					 (.chat != NULL && this isInclude .chat.members)
			create = (.chat == NULL) ||
					 (.chat != NULL && this isInclude .chat.members)
			update = ((.chat == NULL ) || (.chat != NULL && this isInclude .chat.members)) &&
					 ((.created - timestamp) < Time(hour = 5))
			delete = (this == .creator)
		},
		User{
			read = true
		}
	]
}

Message entity{
fields:
    body Struct{
        text String
        attaches Struct{
			fields:
				images [
					Image{
						formats = ["gif", "png", "jpg"]
						thumbs  = {
							name: "preview"
							size: "320x140#"
						}
					}
				]
				videos [Video]
				audios [Audio]
				files  [File]
			configs:
				empty = false
        }
    }
    to User
    reply Message
    chat Chat
    readed Bool{
        value = false
	}
	likes Int!{
		calculated = true			
	}
params:
	defAccess = {
		read   = false
		create = false
		update = false
		delete = false
	}
	routes{
		resource = true
		searchByFields = [.body, .to]				
	}
}

Chat union = GroupChat | ChannelInfo

GroupChat entity{
    members [&User]
    moders [&User]
}

ChannelInfo entity{
	members [&User]
	moders [&User]
}