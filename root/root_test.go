package root

import (
	"bytes"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
)

type dbTest struct {
}

func (d *dbTest) GetAllProjects() ([]map[string]string, error) {
	return []map[string]string{
		{
			"name": "project1",
			"code": "For model{}",
		},
		{
			"name": "project2",
			"code": `For model{}
			//route/{param} GET {
				field1 string	
				field2 string
			}(
				1+1
			)
			`,
		},
	}, nil
}

func (d *dbTest) GetProject(name string) (map[string]string, error) {
	return map[string]string{
		"name": name,
		"code": "For model{}",
	}, nil
}

func (d *dbTest) CreateProject(name string, code string) error {
	if name == "project1" && code == "For model{}" {
		return nil
	}
	return nil
}

func (d *dbTest) UpdateProject(name string, code string) error {
	if name == "project1" && code == "For model{}" {
		return nil
	}
	return fmt.Errorf("%s \n %s", name, code)
}

func (d *dbTest) DeleteProject(name string) error {
	if name == "project1" {
		return nil
	}
	return fmt.Errorf("%s", name)
}

func TestNewRoot(t *testing.T) {
	var dbt dbTest
	r, err := New(&dbt)
	if err != nil {
		t.Errorf("Root init error: %s", err.Error())
	}
	_, ok1 := r.Projects["project1"]
	_, ok2 := r.Projects["project2"]

	if !ok1 || !ok2 {
		t.Errorf("Projects not parsed, expected true && true, get %v && %v", ok1, ok2)
	}
}

func TestApiHandler(t *testing.T) {
	tt := []struct {
		reqPath string
		method  string
		result  string
	}{
		{"/api/project/project1",
			"GET",
			`{"code":"For model{}","name":"project1"}`,
		},
		{"/api/project",
			"POST",
			"status:\"ok\"",
		},
		{"/api/project/project1",
			"PATCH",
			"status:\"ok\"",
		},
		{"/api/project/project1",
			"DELETE",
			"status:\"ok\"",
		},
	}

	var dbt dbTest
	R, err := New(&dbt)

	if err != nil {
		t.Fatalf("init error: %s", err.Error())
	}

	for _, tc := range tt {
		var req *http.Request
		var err error
		if tc.method == "POST" || tc.method == "PATCH" {
			data := url.Values{}
			data.Add("name", "project1")
			data.Add("code", "For model{}")
			b := bytes.NewBuffer([]byte(data.Encode()))
			req, err = http.NewRequest(tc.method, tc.reqPath, b)
			req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
		} else {
			req, err = http.NewRequest(tc.method, tc.reqPath, nil)
		}

		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		R.Router.ServeHTTP(rr, req)

		if rr.Code != http.StatusOK &&
			rr.Body.String() != tc.result {
			t.Errorf("Bad result for request to path %s method %s, expected:\n`%s`\nget:\n`%s`\n",
				tc.reqPath,
				tc.method,
				tc.result,
				rr.Body.String())
		}
	}
}

func TestAllDocsFunc(t *testing.T) {
	res := `<a href="/docs/project1"><h2 class="pink">project1</h2></a><br><a href="/docs/project2"><h2 class="pink">project2</h2></a><br>`
	var dbt dbTest
	R, err := New(&dbt)
	if err != nil {
		t.Fatalf("init error: %s", err.Error())
	}
	route, err := R.Router.Get("getAllDocs").URLPath()
	if err != nil {
		t.Fatalf("path error: %s", err.Error())
	}

	req, err := http.NewRequest("GET", route.Path, nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	R.Router.ServeHTTP(rr, req)
	if rr.Body.String() != res {
		t.Errorf("Bad result for request to path %s, expected:\n`%s`\nget:\n`%s`\n",
			route.Path,
			res,
			rr.Body.String())
	}
}

func TestGetDoc(t *testing.T) {
	res := `<html> 
	<header>
	<link href="https://fonts.googleapis.com/css?family=Roboto+Mono:400,400i,700&amp;subset=cyrillic" rel="stylesheet">
	<style>
	body{
		font-family: 'Roboto Mono', monospace;
		line-height: 1.5;
		font-size: 14px;
	}
	.grey{
		color: #333;
	}
	.pink{
		color: ##D64292;
	}
	.blue{
		color: #1F61A0;
	}
	</style>
	</header>
	 <body>
<h1> Проект project2:</h1><p><span class="grey">URL:</span> <span class="pink">/route/{param}</span><br><span class="grey">Метод:</span> <span class="pink">GET</span><br><span class="grey">Параметры:</span><br><span class="blue">field1: string, nullable </span><br><span class="blue">field2: string, nullable </span></p></body></html>
`
	var dbt dbTest
	R, err := New(&dbt)
	if err != nil {
		t.Fatalf("init error: %s", err.Error())
	}
	route, err := R.Router.Get("getDoc").URL("name", "project2")
	if err != nil {
		t.Fatalf("path error: %s", err.Error())
	}

	req, err := http.NewRequest("GET", route.Path, nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	R.Router.ServeHTTP(rr, req)
	if rr.Body.String() != res {
		t.Errorf("Bad result for request to path %s, expected:\n`%s`\nget:\n`%s`\n",
			route.Path,
			res,
			rr.Body.String())
	}
}

func TestUpdateDoc(t *testing.T) {
	res := `<html> 
	<header>
	<link href="https://fonts.googleapis.com/css?family=Roboto+Mono:400,400i,700&amp;subset=cyrillic" rel="stylesheet">
	<style>
	body{
		font-family: 'Roboto Mono', monospace;
		line-height: 1.5;
		font-size: 14px;
	}
	.grey{
		color: #333;
	}
	.pink{
		color: ##D64292;
	}
	.blue{
		color: #1F61A0;
	}
	</style>
	</header>
	 <body>
<h1> Проект project3:</h1><p><span class="grey">URL:</span> <span class="pink">/route/{param}</span><br><span class="grey">Метод:</span> <span class="pink">GET</span><br><span class="grey">Параметры:</span><br><span class="blue">field1: string, nullable </span><br><span class="blue">field2: string, nullable </span></p></body></html>
`
	newCode := `//route/{param} GET {
		field1 string	
		field2 string
	}(
		1+1
	)
	`
	var dbt dbTest
	R, err := New(&dbt)
	if err != nil {
		t.Fatalf("init error: %s", err.Error())
	}

	testAddNewProject(R, "project3", newCode, t)
	route, err := R.Router.Get("getDoc").URL("name", "project3")
	if err != nil {
		t.Fatalf("path error: %s", err.Error())
	}

	req, err := http.NewRequest("GET", route.Path, nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	R.Router.ServeHTTP(rr, req)
	if rr.Body.String() != res {
		t.Errorf("Bad result for request to path %s, expected:\n`%s`\nget:\n`%s`\n",
			route.Path,
			res,
			rr.Body.String())
	}
}

func testAddNewProject(r *Root, name string, code string, t *testing.T) {
	data := url.Values{}
	data.Add("name", name)
	data.Add("code", code)
	b := bytes.NewBuffer([]byte(data.Encode()))
	req, err := http.NewRequest("POST", "/api/project", b)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	rr := httptest.NewRecorder()
	r.Router.ServeHTTP(rr, req)
	if rr.Code != http.StatusOK {
		t.Fatalf("Bad req to add data: %s", rr.Body.String())
	}
}

/*

func TestServer(t *testing.T) {
	input := `
	//sel/{word} GET {
		word string
		i int
	}(
		len(word) + 1
	)

	//sel/{word} POST {
		word string
	}(
		len(word)
	)

	//sel DELETE {
	}(
		1
	)

	//sel PATCH {
	}(
		{"hi":1}
	)
	`
	type param struct {
		key string
		val string
	}
	tCases := []struct {
		request string
		method  string
		params  []param
		output  string
	}{
		{
			request: addr + "/test/sel/hi",
			method:  "GET",
			params: []param{
				{
					key: "i",
					val: "1",
				},
			},
			output: "3",
		},
		{
			request: addr + "/test/sel/hi",
			method:  "POST",
			output:  "2",
		},
		{
			request: addr + "/test/sel",
			method:  http.MethodDelete,
			output:  "1",
		},
		{
			request: addr + "/test/sel",
			method:  "PATCH",
			output:  `{"hi": 1}`,
		},
	}

	go startServ(input, t)
	duration := time.Second
	time.Sleep(duration)
	for i, tCase := range tCases {
		var body = bytes.NewBufferString("")
		req, err := http.NewRequest(
			tCase.method,
			"http://"+tCase.request,
			body,
		)
		q := req.URL.Query()
		for _, p := range tCase.params {
			q.Add(p.key, p.val)
		}
		req.URL.RawQuery = q.Encode()
		if err != nil {
			t.Fatalf("Case %v. Request init error %v", i, err.Error())
		}

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Fatalf("Case %v. Request error %v", i, err.Error())
		}
		defer resp.Body.Close()

		respBody, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			t.Fatalf("Case %v. Request convert error %v", i, err.Error())
		}

		if string(respBody) != tCase.output {
			t.Errorf("Expected return value \n%s\ngot\n%s", tCase.output, string(respBody))
		}
	}
}

func startServ(input string, t *testing.T) {
	l := lexer.New(input)
	p := parser.New(l)
	proj := p.ParseProgram()
	if len(p.Errors()) != 0 {
		t.Errorf("errors : %v", p.Errors())
		return
	}

	proj.Name = "test"
	rt := New()
	err := rt.AddProject(proj)
	if err != nil {
		t.Errorf("add project error: %s", err)
		return
	}
	srv := &http.Server{
		Handler: rt.Router,
		Addr:    ":8282",
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	err = srv.ListenAndServe()
	if err != nil {
		t.Fatalf("server start error: %s", err.Error())
	}
}
*/
