package root

import (
	"Clerk/db"
	"Clerk/entity"
	"Clerk/evaluator"
	"Clerk/lexer"
	"Clerk/object"
	"Clerk/parser"
	"Clerk/token"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"sync"
	"time"
	//"log"
	"net/http"
	"net/url"
	"strconv"

	"github.com/gorilla/mux"
)

type Root struct {
	sync.Mutex
	Projects map[string]*entity.Project
	Router   *mux.Router
	DB       db.Manager
}

func New(db db.Manager) (*Root, error) {
	var r = Root{
		Projects: make(map[string]*entity.Project),
		DB:       db,
		Router:   mux.NewRouter(),
	}
	projects, err := r.DB.GetAllProjects()
	if err != nil {
		return nil, err
	}

	for _, project := range projects {

		name, ok := project["name"]
		if !ok {
			return nil, fmt.Errorf("Bad project record from database: don't have field name")
		}

		code, ok := project["code"]
		l := lexer.New(code)
		p := parser.New(l)
		project := p.ParseProgram()
		if len(p.Errors()) != 0 {
			return nil, fmt.Errorf("Parse program error: %v", p.Errors())
		}

		project.Name = name
		project.Code = code

		r.Projects[name] = &project
	}

	r.Router = r.getRoutes()
	/*	r.Router.Use(
		func(next http.Handler) http.Handler {
			return r.getRoutes()
		},
	)*/
	return &r, nil
}

func (r *Root) NewProject(name string, code string) error {
	l := lexer.New(code)
	p := parser.New(l)
	project := p.ParseProgram()
	if len(p.Errors()) != 0 {
		return fmt.Errorf("%v", p.Errors())
	}
	project.Name = name
	project.Code = code
	err := r.DB.CreateProject(name, code)
	if err != nil {
		return err
	}
	r.Lock()
	r.Projects[name] = &project
	r.Unlock()
	r.refreshRoutes()
	return nil
}

func (r *Root) GetAllProjects() map[string]*entity.Project {
	return r.Projects
}

func (r *Root) UpdateProjectCode(name string, code string) error {
	err := r.DB.UpdateProject(name, code)
	if err != nil {
		return err
	}
	r.Lock()
	r.Projects[name].Code = code
	r.Unlock()
	r.refreshRoutes()
	return nil
}

func (r *Root) DeleteProject(name string) error {
	err := r.DB.DeleteProject(name)
	if err != nil {
		return nil
	}
	r.Lock()
	delete(r.Projects, name)
	r.Unlock()
	r.refreshRoutes()
	return nil
}

func (r *Root) refreshRoutes() {
	r.Router.Use(
		func(next http.Handler) http.Handler {
			r := r.getRoutes()
			return r
		},
	)
}

func (r *Root) getRoutes() *mux.Router {
	router := r.getProjectsRoutes()
	router.HandleFunc("/api/project/{name}", r.getProjectHandle).
		Methods("GET").
		Name("getProject")
	router.HandleFunc("/api/project", r.createProjectHandle).
		Methods("POST").
		Name("createProject")
	router.HandleFunc("/api/project/{name}", r.updateProjectHandle).
		Methods("PATCH").
		Name("updateProject")
	router.HandleFunc("/api/project/{name}", r.deleteProjectHandle).
		Methods("DELETE").
		Name("deleteProject")
	router.HandleFunc("/docs", r.getAllDocsHandle).
		Methods("GET").
		Name("getAllDocs")
	router.HandleFunc("/docs/{name}", r.getDocHandle).
		Methods("GET").
		Name("getDoc")
	return router
}

func (r *Root) getProjectsRoutes() *mux.Router {
	var router = mux.NewRouter()
	r.Lock()
	for name, project := range r.Projects {
		s := router.PathPrefix("/" + name).Subrouter()
		l := lexer.New(project.Code)
		p := parser.New(l)
		proj := p.ParseProgram()
		if len(p.Errors()) != 0 {
			arr := p.Errors()
			log.Printf("%+v", arr)
			continue
		}
		project = &proj
		var env = object.NewEnvironment()
		for mName, model := range project.Models {
			env.Set(mName, &object.Model{
				Ref: model,
			})
		}
		for rID, route := range project.Routes {
			s.HandleFunc(rID.URI, getHandler(route, *env)).Methods(route.Method)
		}
	}
	r.Unlock()
	return router
}

func getHandler(route entity.Route, env object.Environment) func(http.ResponseWriter, *http.Request) {
	return func(rw http.ResponseWriter, r *http.Request) {
		vars := getVars(r)
		urlVars := mux.Vars(r)
		for k, v := range urlVars {
			vars[k] = v
		}
		//log.Fatalf(" vars %v", vars)
		for _, field := range route.Fields {
			sVal, ok := vars[field.Name]
			if !ok && field.NonNullable {
				http.Error(
					rw,
					fmt.Sprintf("Missing required parameter %s", field.Name),
					http.StatusBadRequest,
				)
				return
			}
			val, err := convToType(sVal, field.FieldType)
			if err != nil {
				http.Error(
					rw,
					fmt.Sprintf("field %s: %s", field.Name, err.Error()),
					http.StatusBadRequest,
				)
				return
			}
			err = evaluator.СheckForCorrectly(val, field.Configs, object.NewEnvironment())
			if err != nil {
				http.Error(
					rw,
					fmt.Sprintf("field %s: %s", field.Name, err.Error()),
					http.StatusBadRequest,
				)
				return
			}
			obj, err := object.ConvertToObject(val)
			if err != nil {
				http.Error(
					rw,
					fmt.Sprintf("field %s: %s", field.Name, err.Error()),
					http.StatusBadRequest,
				)
				return
			}
			env.Set(field.Name, obj)
		}

		res := evaluator.Eval(route.Action, &env)
		fmt.Fprintf(rw, res.Inspect())
	}
}

func (rt *Root) getProjectHandle(w http.ResponseWriter, r *http.Request) {
	vars := getVars(r)
	urlVars := mux.Vars(r)
	for k, v := range urlVars {
		vars[k] = v
	}
	name, ok := vars["name"]
	if !ok {
		http.Error(w, "Missing parameter 'name'", http.StatusBadRequest)
		return
	}

	rt.Lock()
	project, ok := rt.Projects[name]
	rt.Unlock()

	if !ok {
		fmt.Fprint(w, `{status:"Undefined project"}`)
		return
	}

	var res = make(map[string]string)

	res["name"] = project.Name
	res["code"] = project.Code

	json, err := json.Marshal(res)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	fmt.Fprint(w, string(json))
}

func (rt *Root) createProjectHandle(w http.ResponseWriter, r *http.Request) {
	vars := getVars(r)
	urlVars := mux.Vars(r)
	for k, v := range urlVars {
		vars[k] = v
	}
	name, ok := vars["name"]
	if !ok {
		http.Error(w, `{"error":"Missing parameter 'name'"}`, http.StatusBadRequest)
		return
	}

	code, ok := vars["code"]
	if !ok {
		code = ""
	}

	err := rt.NewProject(name, code)

	if err != nil {
		res := fmt.Sprintf(`{"error":"%s"}`, err.Error())
		http.Error(w, res, http.StatusInternalServerError)
		return
	}
	fmt.Fprint(w, "status:\"ok\"")
}

func (rt *Root) updateProjectHandle(w http.ResponseWriter, r *http.Request) {
	vars := getVars(r)
	urlVars := mux.Vars(r)
	for k, v := range urlVars {
		vars[k] = v
	}
	name, ok := vars["name"]
	if !ok {
		http.Error(w, `{"error":"Missing parameter 'name'"}`, http.StatusBadRequest)
		return
	}

	code, ok := vars["code"]
	if !ok {
		http.Error(w, `{"error":"Missing parameter 'code'"}`, http.StatusBadRequest)
	}

	err := rt.UpdateProjectCode(name, code)

	if err != nil {
		res := fmt.Sprintf(`{"error":"%s"}`, err.Error())
		http.Error(w, res, http.StatusInternalServerError)
		return
	}
	fmt.Fprint(w, "status:\"ok\"")
}

func (rt *Root) deleteProjectHandle(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	name, ok := vars["name"]
	if !ok {
		http.Error(w, `{"error":"Missing parameter 'name'"}`, http.StatusBadRequest)
		return
	}

	err := rt.DeleteProject(name)

	if err != nil {
		res := fmt.Sprintf(`{"error":"%s"}`, err.Error())
		http.Error(w, res, http.StatusInternalServerError)
		return
	}
	fmt.Fprint(w, "status:\"ok\"")
}


func (rt *Root) getAllDocsHandle(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	for name := range rt.Projects {
		route, err := rt.Router.Get("getDoc").URL("name", name)
		if err != nil {
			msg := fmt.Sprintf(`{"error":"%s"}`, err.Error())
			http.Error(w, msg, http.StatusBadRequest)
			return
		}
		fmt.Fprint(w, "<a href=\""+route.Path+"\"><h2 class=\"pink\">"+name+"</h2></a><br>")
	}
}

func (rt *Root) getDocHandle(w http.ResponseWriter, r *http.Request) {
	//Не забывай закрывать html и body тэг в конце
	fmt.Fprintln(w, "<html>", getHTMLHeader(), "<body>")
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	vars := mux.Vars(r)

	name, ok := vars["name"]
	if !ok {
		http.Error(w, "Missing parameter 'name'", http.StatusBadRequest)
		return
	}

	project, ok := rt.Projects[name]
	if !ok {
		msg := fmt.Sprintf("Missing project '%s'", name)
		http.Error(w, msg, http.StatusBadRequest)
		return
	}

	fmt.Fprint(w, "<h1> Проект "+name+":</h1>")
	for rid, route := range project.Routes {
		txt := fmt.Sprintf(`<p><span class="grey">URL:</span> <span class="pink">%s</span><br><span class="grey">Метод:</span> <span class="pink">%s</span><br>`,
			rid.URI,
			rid.Method)
		fmt.Fprint(w, txt)
		fmt.Fprintf(w, `<span class="grey">Параметры:</span>`)
		for _, f := range route.Fields {
			txt = fmt.Sprintf(`<br><span class="blue">%s: %s, %s </span>`,
				f.Name,
				f.FieldType.Literal,
				nonNullableString(f.NonNullable))
			fmt.Fprint(w, txt)
		}
		fmt.Fprint(w, "</p>")
	}
	fmt.Fprintln(w, "</body></html>")
}
func nonNullableString(b bool) string {
	if b {
		return "non nullable"
	} else {
		return "nullable"
	}
}
func CallNewProject(address string, name string, code string) error {
	data := url.Values{}
	data.Add("name", name)
	data.Add("code", code)
	b := bytes.NewBuffer([]byte(data.Encode()))
	req, err := http.NewRequest("POST", address+"/api/project", b)
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	tr := &http.Transport{
		MaxIdleConns:       10,
		IdleConnTimeout:    5 * time.Second,
		DisableCompression: true,
	}
	client := &http.Client{Transport: tr}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		respBody, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return fmt.Errorf("Can't read request error: %s ", err.Error())
		}
		return fmt.Errorf("Bad req to create project: %s", string(respBody))
	}
	return nil
}

func convToType(val string, convTo token.Token) (interface{}, error) {
	switch convTo.Type {
	case token.STRING_TYPE:
		return val, nil
	case token.INT_TYPE:
		return strconv.ParseInt(val, 10, 64)
	case token.FLOAT_TYPE:
		return strconv.ParseFloat(val, 64)
	case token.BOOL_TYPE:
		return strconv.ParseBool(val)
	default:
		return nil, fmt.Errorf("Undefined type of params")
	}
}

func getVars(r *http.Request) map[string]string {
	var vars map[string]string
	if r.Method == "GET" {
		v := r.URL.Query()
		vars = convArrToStr(v)
	} else {
		r.ParseForm()
		v := r.Form
		vars = convArrToStr(v)
	}
	return vars
}

func convArrToStr(strs map[string][]string) map[string]string {
	var res = make(map[string]string)
	for key, val := range strs {
		if len(val) == 1 {
			res[key] = val[0]
			continue
		}
		var arr string
		arr = "{"
		for i, item := range val {
			arr += "\"" + item + "\""
			if i+1 == len(val) {
				arr += ","
			}
		}
		arr += "}"
		res[key] = arr
	}
	return res
}

func getHTMLHeader() string {
	return `
	<header>
	<link href="https://fonts.googleapis.com/css?family=Roboto+Mono:400,400i,700&amp;subset=cyrillic" rel="stylesheet">
	<style>
	body{
		font-family: 'Roboto Mono', monospace;
		line-height: 1.5;
		font-size: 14px;
	}
	.grey{
		color: #333;
	}
	.pink{
		color: ##D64292;
	}
	.blue{
		color: #1F61A0;
	}
	</style>
	</header>
	`
}
