package token

//token types
const (
	ILLEGAL TokenType = TokenType(iota + 1) // error name
	EOF                                     // end of file

	IDENT
	INT
	STRING
	FLOAT
	BOOL
	URI
	INT_TYPE
	STRING_TYPE
	FLOAT_TYPE
	BOOL_TYPE
	IF
	ELSE

	// Operators
	ASSIGN   // =
	PLUS     // +
	MINUS    // -
	ASTERISK // *
	SLASH    // /

	LT // <
	GT // >

	EQ     // ==
	NOT_EQ // !=

	// Delimiters
	DOT      // .
	COMMA    // ,
	LPAREN   // (
	RPAREN   // )
	LBRACKET // [
	RBRACKET // ]
	LBRACE   // {
	RBRACE   // }
	COLON    // :
	BANG     // !
	AT       // @

	// Keywords
	TRUE
	FALSE
)

type TokenType int

type Token struct {
	Type    TokenType
	Literal string
}

func (t TokenType) ToString() string {
	switch t {
	case EOF:
		return "EOF"
	case IDENT:
		return "IDENT"
	case INT:
		return "int"
	case STRING:
		return "string"
	case FLOAT:
		return "float"
	case BOOL:
		return "bool"
	case URI:
		return "URI"
	case INT_TYPE:
		return "int type"
	case STRING_TYPE:
		return "string type"
	case FLOAT_TYPE:
		return "float type"
	case BOOL_TYPE:
		return "bool type"
	case IF:
		return "if"
	case ELSE:
		return "else"
	case ASSIGN:
		return "="
	case PLUS:
		return "+"
	case MINUS:
		return "-"
	case ASTERISK:
		return "*"
	case SLASH:
		return "/"
	case LT:
		return "<"
	case GT:
		return ">"
	case EQ:
		return "=="
	case NOT_EQ:
		return "!="
	case DOT:
		return "."
	case COMMA:
		return ","
	case LPAREN:
		return "("
	case RPAREN:
		return ")"
	case LBRACKET: // [
		return "["
	case RBRACKET: // ]
		return "]"
	case LBRACE:
		return "{"
	case RBRACE:
		return "}"
	case COLON:
		return ":"
	case BANG:
		return "!"
	case TRUE:
		return "true"
	case FALSE:
		return "false"
	default:
		return "UNDEFINED"
	}
}

var keywords = map[string]TokenType{
	"true":   TRUE,
	"false":  FALSE,
	"if":     IF,
	"else":   ELSE,
	"int":    INT_TYPE,
	"string": STRING_TYPE,
	"bool":   BOOL_TYPE,
	"float":  FLOAT_TYPE,
}

// Проверяет на принадлежность keywords
func LookupIdent(ident string) TokenType {
	if tok, ok := keywords[ident]; ok {
		return tok
	}
	return IDENT
}
