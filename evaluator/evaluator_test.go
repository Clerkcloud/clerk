package evaluator

import (
	"Clerk/lexer"
	"Clerk/object"
	"Clerk/parser"
	"testing"
)

func TestEvalIntegerExpression(t *testing.T) {
	tests := []struct {
		input    string
		expected int64
	}{
		{"5", 5},
		{"10", 10},
		{"-5", -5},
		{"-10", -10},
		{"5 + 5 + 5 + 5 - 10", 10},
		{"2 * 2 * 2 * 2 * 2", 32},
		{"-50 + 100 + -50", 0},
		{"5 * 2 + 10", 20},
		{"5 + 2 * 10", 25},
		{"20 + 2 * -10", 0},
		{"50 / 2 * 2 + 10", 60},
		{"2 * (5 + 10)", 30},
		{"3 * 3 * 3 + 10", 37},
		{"3 * (3 * 3) + 10", 37},
		{"(5 + 10 * 2 + 15 / 3) * 2 + -10", 50},
	}
	for _, tt := range tests {
		evaluated := testEval(tt.input)
		testIntegerObject(t, evaluated, tt.expected)
	}
}

func TestEvalBooleanExpression(t *testing.T) {
	tests := []struct {
		input    string
		expected bool
	}{
		{"true == true", true},
		{"false == false", true},
		{"true == false", false},
		{"true != false", true},
		{"false != true", true},
		{"(1 < 2) == true", true},
		{"(1 < 2) == false", false},
		{"(1 > 2) == true", false},
		{"(1 > 2) == false", true},
	}
	for _, tt := range tests {
		evaluated := testEval(tt.input)
		testBooleanObject(t, evaluated, tt.expected)
	}
}

func TestBangOperator(t *testing.T) {
	tests := []struct {
		input    string
		expected bool
	}{
		{"!true", false},
		{"!false", true},
		{"!5", false},
		{"!!true", true},
		{"!!false", false},
		{"!!5", true},
	}
	for _, tt := range tests {
		evaluated := testEval(tt.input)
		testBooleanObject(t, evaluated, tt.expected)
	}
}

func TestIfElseExpressions(t *testing.T) {
	tests := []struct {
		input    string
		expected interface{}
	}{
		{"if (true) { 10 }", 10},
		{"if (false) { 10 }", nil},
		{"if (1) { 10 }", 10},
		{"if (1 < 2) { 10 }", 10},
		{"if (1 > 2) { 10 }", nil},
		{"if (1 > 2) { 10 } else { 20 }", 20},
		{"if (1 < 2) { 10 } else { 20 }", 10},
	}
	for _, tt := range tests {
		evaluated := testEval(tt.input)
		integer, ok := tt.expected.(int)
		if ok {
			testIntegerObject(t, evaluated, int64(integer))
		} else {
			testNullObject(t, evaluated)
		}
	}
}

/*
func TestEvalNominalExpression(t *testing.T) {
	tests := []struct {
		input    string
		expected object.NomPair
	}{
		{
			input: ":k > 20",
			expected: object.NomPair{
				Nominal:    object.Nominal{Value: "k"},
				Operator:   ">",
				AnotherVal: &object.Integer{Value: 20},
			},
		},
		{
			input: "20 < :k",
			expected: object.NomPair{
				Nominal:    object.Nominal{Value: "k"},
				Operator:   ">",
				AnotherVal: &object.Integer{Value: 20},
			},
		},
	}
	for _, tt := range tests {
		obj := testEval(tt.input)
		testNomPairObject(t, obj, tt.expected)
	}
}
*/

func testEval(input string) object.Object {
	l := lexer.New(input)
	p := parser.New(l)
	exp := p.ParseExpression()
	env := object.NewEnvironment()
	return Eval(exp, env)
}

func testIntegerObject(t *testing.T, obj object.Object, expected int64) bool {
	result, ok := obj.(*object.Integer)
	if !ok {
		t.Errorf("object is not Integer. got=%T (%+v)", obj, obj)
		return false
	}
	if result.Value != expected {
		t.Errorf("object has wrong value. got=%d, want=%d",
			result.Value, expected)
		return false
	}
	return true
}

func testBooleanObject(t *testing.T, obj object.Object, expected bool) bool {
	result, ok := obj.(*object.Boolean)
	if !ok {
		t.Errorf("object is not Boolean. got=%T (%+v)", obj, obj)
		return false
	}
	if result.Value != expected {
		t.Errorf("object has wrong value. got=%t, want=%t",
			result.Value, expected)
		return false
	}
	return true
}

/*
func testNomPairObject(t *testing.T, obj object.Object, expected object.NomPair) bool {
	result, ok := obj.(*object.NomPair)
	if !ok {
		t.Errorf("object is not NomPair. got=%T (%+v)", obj, obj)
		return false
	}
	if result.Nominal.Value != expected.Nominal.Value {
		t.Errorf("Nominal value has wrong value, got=%s, want=%s",
			result.Nominal.Value,
			expected.Nominal.Value)
		return false
	}
	if result.Operator != expected.Operator {
		t.Errorf("Wrong operator, got=%s, want=%s",
			result.Operator,
			expected.Operator)
		return false
	}
	if result.AnotherVal.Inspect() != expected.AnotherVal.Inspect() {
		t.Errorf("AnotherVal has wrong valur, got=%s, want=%s",
			result.AnotherVal.Inspect(),
			expected.AnotherVal.Inspect())
		return false
	}
	return true
}
*/

func testNullObject(t *testing.T, obj object.Object) bool {
	if obj != NULL {
		t.Errorf("object is not NULL. got=%T (%+v)", obj, obj)
		return false
	}
	return true
}

func TestStringLiteral(t *testing.T) {
	input := `"Hello World!"`
	evaluated := testEval(input)
	str, ok := evaluated.(*object.String)
	if !ok {
		t.Fatalf("object is not String. got=%T (%+v)", evaluated, evaluated)
	}
	if str.Value != "Hello World!" {
		t.Errorf("String has wrong value. got=%q", str.Value)
	}
}

func TestStringConcatenation(t *testing.T) {
	input := `"Hello" + " " + "World!"`
	evaluated := testEval(input)
	str, ok := evaluated.(*object.String)
	if !ok {
		t.Fatalf("object is not String. got=%T (%+v)", evaluated, evaluated)
	}
	if str.Value != "Hello World!" {
		t.Errorf("String has wrong value. got=%q", str.Value)
	}
}

func TestErrorHandling(t *testing.T) {
	tests := []struct {
		input           string
		expectedMessage string
	}{
		{
			"5 + true",
			"type mismatch: INTEGER + BOOLEAN",
		},
		{
			"5 + true",
			"type mismatch: INTEGER + BOOLEAN",
		},
		{
			"-true",
			"unknown operator: -BOOLEAN",
		},
		{
			"true + false",
			"unknown operator: BOOLEAN + BOOLEAN",
		},
		{
			"if (10 > 1) { true + false}",
			"unknown operator: BOOLEAN + BOOLEAN",
		},
		{
			"foobar",
			"identifier not found: foobar",
		},
	}
	for i, tt := range tests {
		evaluated := testEval(tt.input)
		errObj, ok := evaluated.(*object.Error)
		if !ok {
			t.Errorf("case %v: no error object returned. got=%T(%+v)",
				i, evaluated, evaluated)
			continue
		}
		if errObj.Message != tt.expectedMessage {
			t.Errorf("wrong error message. expected=%q, got=%q",
				tt.expectedMessage, errObj.Message)
		}
	}
}

func TestEnvironment(t *testing.T) {
	var envs = object.NewEnvironment()
	var a = object.Integer{
		Value: 100,
	}
	var b = object.Integer{
		Value: 120,
	}
	envs.Set("a", &a)
	envs.Set("b", &b)
	var code = "a + b"
	var l = lexer.New(code)
	var p = parser.New(l)
	var exp = p.ParseExpression()
	var answ = Eval(exp, envs)
	ans, ok := answ.(*object.Integer)
	if !ok {
		t.Fatalf("object is not Integer. got=%T (%+v)", answ, answ)
	}

	if ans.Value != 220 {
		t.Errorf("answer value not 220, got = %v", ans.Value)
	}

}

func TestBuiltinFunctions(t *testing.T) {
	tests := []struct {
		input    string
		expected interface{}
	}{
		{`len("")`, 0},
		{`len("four")`, 4},
		{`len("hello world")`, 11},
		{`len(1)`, "argument to `len` not supported, got INTEGER"},
		{`len("one", "two")`, "wrong number of arguments. got=2, want=1"},
	}
	for _, tt := range tests {
		evaluated := testEval(tt.input)
		switch expected := tt.expected.(type) {
		case int:
			testIntegerObject(t, evaluated, int64(expected))
		case string:
			errObj, ok := evaluated.(*object.Error)
			if !ok {
				t.Errorf("object is not Error. got=%T (%+v)",
					evaluated, evaluated)
				continue
			}
			if errObj.Message != expected {
				t.Errorf("wrong error message. expected=%q, got=%q",
					expected, errObj.Message)
			}
		}
	}
}

func TestMethods(t *testing.T) {
	tests := []struct {
		input    string
		expected interface{}
	}{
		{`"".len()`, 0},
		{`"four".len()`, 4},
		{`"hello world".len()`, 11},
		{`"hello world".go()`, "understand string method: go"},
		{`1.len()`, "not supported type of object: INTEGER"},
	}
	for _, tt := range tests {
		evaluated := testEval(tt.input)
		switch expected := tt.expected.(type) {
		case int:
			testIntegerObject(t, evaluated, int64(expected))
		case string:
			errObj, ok := evaluated.(*object.Error)
			if !ok {
				t.Errorf("object is not Error. got=%T (%+v)",
					evaluated, evaluated)
				continue
			}
			if errObj.Message != expected {
				t.Errorf("wrong error message. expected=%q, got=%q",
					expected, errObj.Message)
			}
		}
	}
}

func TestArrayLiterals(t *testing.T) {
	input := "[1, 2 * 2, 3 + 3]"
	evaluated := testEval(input)
	result, ok := evaluated.(*object.Array)
	if !ok {
		t.Fatalf("object is not Array. got=%T (%+v)", evaluated, evaluated)
	}
	if len(result.Elements) != 3 {
		t.Fatalf("array has wrong num of elements. got=%d",
			len(result.Elements))
	}
	testIntegerObject(t, result.Elements[0], 1)
	testIntegerObject(t, result.Elements[1], 4)
	testIntegerObject(t, result.Elements[2], 6)
}

func TestArrayIndexExpressions(t *testing.T) {
	tests := []struct {
		input    string
		expected interface{}
	}{
		{
			"[1, 2, 3][0]",
			1,
		},
		{
			"[1, 2, 3][1]",
			2,
		},
		{
			"[1, 2, 3][2]",
			3,
		},
		{
			"[1, 2, 3][1 + 1];",
			3,
		},
		{
			"[1, 2, 3][3]",
			nil,
		},
		{
			"[1, 2, 3][-1]",
			nil,
		},
		{
			"[1, 2, 3].len()",
			3,
		},
		{
			"[1, 2, 3].push(2).len()",
			4,
		},
	}
	for _, tt := range tests {
		evaluated := testEval(tt.input)
		integer, ok := tt.expected.(int)
		if ok {
			testIntegerObject(t, evaluated, int64(integer))
		} else {
			testNullObject(t, evaluated)
		}
	}
}

func TestHashLiterals(t *testing.T) {
	input := `
	{
	"one": 10 - 9,
	"thr" + "ee": 6 / 2,
	4: 4,
	true: 5,
	false: 6
	}`
	evaluated := testEval(input)
	result, ok := evaluated.(*object.Hash)
	if !ok {
		t.Fatalf("Eval didn't return Hash. got=%T (%+v)", evaluated, evaluated)
	}
	expected := map[object.HashKey]int64{
		(&object.String{Value: "one"}).HashKey():   1,
		(&object.String{Value: "three"}).HashKey(): 3,
		(&object.Integer{Value: 4}).HashKey():      4,
		TRUE.HashKey():                             5,
		FALSE.HashKey():                            6,
	}
	if len(result.Pairs) != len(expected) {
		t.Fatalf("Hash has wrong num of pairs. got=%d", len(result.Pairs))
	}
	for expectedKey, expectedValue := range expected {
		pair, ok := result.Pairs[expectedKey]
		if !ok {
			t.Errorf("no pair for given key in Pairs")
		}
		testIntegerObject(t, pair.Value, expectedValue)
	}
}

func testModelEnv(input string, t *testing.T) *object.Environment {
	l := lexer.New(input)
	p := parser.New(l)
	proj := p.ParseProgram()
	proj.Name = "testing"
	if len(p.Errors()) != 0 {
		t.Fatalf(" Parse errors %+v ", p.Errors)
	}

	env := object.NewEnvironment()
	for k, m := range proj.Models {
		env.Set(k,
			&object.Model{
				Ref: m,
			},
		)
	}

	return env
}

func TestModelCRUDtMethods(t *testing.T) {
	input := `
	Test model{
	fields:
		field string{
			maxLen: 6
			minLen: 2
		}
		field2 int{
			defValue: 2
		}
		field3 bool
	configs:
	database.driver: "mongo"
	database.address: "localhost"
	}
	`

	tCases := []struct {
		create string
		upd    string
		sel    string
		del    string
	}{
		{
			create: `Test.create(
				{
					"field": "fTest"
				}
			)`,
			upd: `Test.update(
					{
						"field": "fTest"
					},
					{
						"field": "f2Test"
					}
				)`,
			sel: `Test.select(
				{
					"field": "f2Test"
				}
			)`,
			del: `Test.delete(
				{
					"field": "f2Test"
				}
			)`,
		},
	}

	env := testModelEnv(input, t)
	for _, tCase := range tCases {
		guid := testModelCreate(tCase.create, env, t)
		testModelUpdate(tCase.upd, env, t)
		res := testModelSelect(tCase.sel, env, t)
		testModelDelete(tCase.del, env, t)
		if guid != res {
			t.Errorf("Bad guid, need %s, get %s", guid, res)
		}
		c := getCountOfSelect(tCase.sel, env, t)
		if c != 0 {
			t.Errorf("Bad count of results, need 0, got %v", c)
		}
	}
}

func testModelCreate(create string, env *object.Environment, t *testing.T) string {
	l := lexer.New(create)
	p := parser.New(l)
	exp := p.ParseExpression()
	res := Eval(exp, env)
	if res.Type() == object.ERROR_OBJ {
		t.Errorf("result error: %s", res.Inspect())
	} else if res.Type() == object.STRING_OBJ {
		return res.(*object.String).Value
	} else {
		t.Fatalf("Unefined type of result, need string %v", res)
	}
	return ""
}

func testModelUpdate(update string, env *object.Environment, t *testing.T) {
	l := lexer.New(update)
	p := parser.New(l)
	exp := p.ParseExpression()
	res := Eval(exp, env)
	if res.Type() == object.ERROR_OBJ {
		t.Fatalf("result error: %s", res.Inspect())
	}
}

func testModelDelete(update string, env *object.Environment, t *testing.T) {
	l := lexer.New(update)
	p := parser.New(l)
	exp := p.ParseExpression()
	res := Eval(exp, env)
	if res.Type() == object.ERROR_OBJ {
		t.Fatalf("result error: %s", res.Inspect())
	}
}

func testModelSelect(sel string, env *object.Environment, t *testing.T) string {
	l := lexer.New(sel)
	p := parser.New(l)
	exp := p.ParseExpression()
	res := Eval(exp, env)
	if res.Type() == object.ERROR_OBJ {
		t.Fatalf("result error: %s", res.Inspect())
	} else if res.Type() == object.ARRAY_OBJ {
		hash := res.(*object.Array).Elements[0].(*object.Hash).Pairs
		hKey, _ := object.GetHashKey(&object.String{Value: "guid"})
		return hash[hKey].Value.(*object.String).Value
	} else {
		t.Fatalf("Undefined type of result, need string, get %v", res)
	}
	return ""
}

func getCountOfSelect(sel string, env *object.Environment, t *testing.T) int {
	l := lexer.New(sel)
	p := parser.New(l)
	exp := p.ParseExpression()
	res := Eval(exp, env)
	if res.Type() == object.ERROR_OBJ {
		t.Fatalf("result error: %s", res.Inspect())
	}
	if res.Type() != object.ARRAY_OBJ {
		t.Fatalf("Bad type of object, expect ARRAY_OBJ, got: %s", res.Type())
	}
	arr := res.(*object.Array).Elements
	return len(arr)
}
