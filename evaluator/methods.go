package evaluator

import (
	"Clerk/dbms"
	"Clerk/entity"
	"Clerk/object"
	"Clerk/token"
	"fmt"
)

type strMethod func(o *object.String) *object.Builtin
type arrMethod func(o *object.Array) *object.Builtin
type modMethod func(o *object.Model) *object.Builtin

func getStringMethods() map[string]strMethod {
	return map[string]strMethod{
		"len": func(o *object.String) *object.Builtin {
			return &object.Builtin{
				Fn: func(args ...object.Object) object.Object {
					if len(args) != 0 {
						return newError("wrong number of arguments. got=%d, want=0",
							len(args))
					}
					return &object.Integer{
						Value: int64(
							len([]rune(o.Value)),
						),
					}
				},
			}
		},
	}
}

func getArrayMethods() map[string]arrMethod {
	return map[string]arrMethod{
		"len": func(arr *object.Array) *object.Builtin {
			return &object.Builtin{
				Fn: func(args ...object.Object) object.Object {
					if len(args) != 0 {
						return newError("wrong number of arguments. got=%d, want=0",
							len(args))
					}
					return &object.Integer{
						Value: int64(
							len(arr.Elements),
						),
					}
				},
			}
		},
		"push": func(arr *object.Array) *object.Builtin {
			return &object.Builtin{
				Fn: func(args ...object.Object) object.Object {
					if len(args) != 1 {
						return newError("wrong number of arguments. got=%d, want=1",
							len(args))
					}
					length := len(arr.Elements)
					newElements := make([]object.Object, length+1, length+1)
					copy(newElements, arr.Elements)
					newElements[length] = args[0]
					return &object.Array{Elements: newElements}
				},
			}
		},
	}

}

func getModelMethods() map[string]modMethod {
	return map[string]modMethod{
		"create": func(model *object.Model) *object.Builtin {
			return &object.Builtin{
				Fn: func(args ...object.Object) object.Object {
					if len(args) != 1 {
						return newError("wrong number of arguments. got=%d, expected=1",
							len(args))
					}
					var arg = args[0]
					if arg.Type() != object.HASH_OBJ {
						return newError("wrong type of argument. got=%s, expected=%s",
							arg.Type(),
							object.HASH_OBJ,
						)
					}
					var hash = arg.(*object.Hash)

					res, err := createModel(model.Ref, hash)
					if err != nil {
						return newError(err.Error())
					}
					return &object.String{
						Value: res,
					}
				},
			}
		},
		"select": func(model *object.Model) *object.Builtin {
			return &object.Builtin{
				Fn: func(args ...object.Object) object.Object {
					if len(args) != 1 {
						return newError("wrong number of arguments. got=%d, expected=1",
							len(args))
					}
					var arg = args[0]
					if arg.Type() != object.HASH_OBJ {
						return newError("wrong type of argument. got=%s, expected=%s",
							arg.Type(),
							object.HASH_OBJ,
						)
					}
					var hash = arg.(*object.Hash)

					dbRes, err := selectModel(model.Ref, hash)
					if err != nil {
						return newError(err.Error())
					}
					res, err := object.ConvertToObject(dbRes)
					if err != nil {
						return newError(err.Error())
					}
					return res
				},
			}
		},
		"update": func(model *object.Model) *object.Builtin {
			return &object.Builtin{
				Fn: func(args ...object.Object) object.Object {
					if len(args) != 2 {
						return newError("wrong number of arguments. got=%d, expected=2",
							len(args))
					}
					var selArg = args[0]
					if selArg.Type() != object.HASH_OBJ {
						return newError("wrong type of argument. got=%s, expected=%s",
							selArg.Type(),
							object.HASH_OBJ,
						)
					}
					var selHash = selArg.(*object.Hash)

					var updArg = args[1]
					if updArg.Type() != object.HASH_OBJ {
						return newError("wrong type of argument. got=%s, expected=%s",
							updArg.Type(),
							object.HASH_OBJ,
						)
					}
					var updHash = updArg.(*object.Hash)

					dbSelRes, err := selectModel(model.Ref, selHash)
					if err != nil {
						return newError(err.Error())
					}

					var selRes []object.Hash
					for _, hash := range dbSelRes {
						HashObj, err := object.ConvertToObject(hash)
						if err != nil {
							return newError(err.Error())
						}
						selRes = append(selRes, *HashObj.(*object.Hash))
					}

					err = updateModel(model.Ref, selRes, updHash)
					if err != nil {
						return newError(err.Error())
					}
					obj, err := object.ConvertToObject(err)
					return obj
				},
			}
		},
		"delete": func(model *object.Model) *object.Builtin {
			return &object.Builtin{
				Fn: func(args ...object.Object) object.Object {
					if len(args) != 1 {
						return newError("wrong number of arguments. got=%d, expected=1",
							len(args))
					}
					var arg = args[0]
					if arg.Type() != object.HASH_OBJ {
						return newError("wrong type of argument. got=%s, expected=%s",
							arg.Type(),
							object.HASH_OBJ,
						)
					}
					var hash = arg.(*object.Hash)

					err := deleteModel(&model.Ref, hash)
					res, err := object.ConvertToObject(err)
					if err != nil {
						return &object.Error{
							Message: err.Error(),
						}
					}
					return res
				},
			}
		},
	}
}

func createModel(model entity.Model, hash *object.Hash) (string, error) {
	if isHaveParamGUID(hash) {
		return "", fmt.Errorf("Bad parameter 'guid' for creating model")
	}

	err := checkNullAndUnnecess(hash, &model)
	if err != nil {
		return "", err
	}
	vals, err := convertAndCheckValues(hash, &model)
	if err != nil {
		return "", err
	}

	db, err := getDB(&model)

	if err != nil {
		return "", err
	}

	return db.Create(vals)
}

func selectModel(model entity.Model, hash *object.Hash) ([]map[string]interface{}, error) {

	vals, err := convertValues(hash, &model)
	if err != nil {
		return nil, err
	}

	db, err := getDB(&model)

	if err != nil {
		return nil, err
	}

	return db.Select(vals)
}

func updateModel(model entity.Model, selHash []object.Hash, updHash *object.Hash) error {

	err := checkNullAndUnnecess(updHash, &model)
	if err != nil {
		return err
	}

	newHashes := mergeHash(selHash, updHash)
	if err != nil {
		return err
	}
	var errs []error
	for _, newHash := range newHashes {
		vals, err := convertAndCheckValues(&newHash, &model)
		if err != nil {
			return err
		}

		db, err := getDB(&model)

		if err != nil {
			return err
		}
		var key map[string]interface{}
		key = make(map[string]interface{})
		key["guid"] = vals["guid"]
		delete(vals, "guid")
		err = db.Update(key, vals)
		if err != nil {
			errs = append(
				errs,
				fmt.Errorf("Record %s: %s", key["guid"], err.Error()),
			)
		}
	}
	if len(errs) != 0 {
		return fmt.Errorf("%v", errs)
	}

	return nil
}

func deleteModel(model *entity.Model, hash *object.Hash) error {
	vals, err := convertValues(hash, model)
	if err != nil {
		return err
	}

	db, err := getDB(model)

	if err != nil {
		return err
	}

	return db.Delete(vals)
}

//Возвращает присутствие параметра GUID в хеше
func isHaveParamGUID(hash *object.Hash) bool {
	val, err := object.GetHashKey(
		&object.String{
			Value: "guid",
		},
	)
	if err != nil {
		return false
	}
	_, ok := hash.Pairs[val]
	return ok
}

//Проверяет поля на присутствие указателей на null там, где поле nonNullable
//и проверяет на присутствие лишних полей
func checkNullAndUnnecess(hash *object.Hash, model *entity.Model) error {
	for _, pair := range hash.Pairs {
		fieldName, ok := pair.Key.(*object.String)
		if !ok {
			return fmt.Errorf("Bad type name of field: expected STRING_OBJ, got=%s", pair.Key.Type())
		}
		mField, ok := model.GetField(fieldName.Value)
		if !ok {
			return fmt.Errorf("Non-existent fields name %s", fieldName.Value)
		}
		if pair.Value.Type() == object.NULL_OBJ &&
			mField.NonNullable {
			return fmt.Errorf("Null value for non nullable field %s", fieldName.Value)
		}
	}
	return nil
}

//Переводит значения из object в нужный тип и сохраняет в interface
//если поле не имеет значения, но у него установлен defVal, устанавливает его
//проверяет полученное значение на соотвествие параматрам
func convertAndCheckValues(hash *object.Hash, model *entity.Model) (map[string]interface{}, error) {
	var result = make(map[string]interface{})
	var env = object.NewEnvironment()
	for _, field := range model.Fields {
		hKey, err := object.GetHashKey(
			&object.String{
				Value: field.Name,
			},
		)
		if err != nil {
			return result, err
		}

		pair, ok := hash.Pairs[hKey]
		if !ok {
			err = checkFieldDefValue(&field, env, hash)
			if err != nil {
				return result, err
			}
		}

		pair, ok = hash.Pairs[hKey]
		if !ok {
			continue
		}

		val, err := convValue(pair.Value, &field)
		if err != nil {
			return result, err
		}

		err = СheckForCorrectly(val, field.Configs, env)
		if err != nil {
			return result, err
		}

		valObj, err := object.ConvertToObject(val)

		if err != nil {
			return result, err
		}

		result[field.Name] = val
		env.Set(field.Name, valObj)
	}

	return result, nil
}

//Переводит значения из object в нужный тип и сохраняет в interface
func convertValues(hash *object.Hash, model *entity.Model) (map[string]interface{}, error) {
	var result = make(map[string]interface{})
	for _, pair := range hash.Pairs {
		fName, ok := pair.Key.(*object.String)
		if !ok {
			return result, fmt.Errorf("Can't convert %v to string", pair.Key)
		}
		field, ok := model.GetField(fName.Value)
		if !ok {
			return result, fmt.Errorf("Non-existent fields name %s", fName.Value)
		}

		val, err := convValue(pair.Value, field)
		if err != nil {
			return result, err
		}

		result[field.Name] = val
	}

	return result, nil
}

//Если поле не имеет значения, но у него установлен defVal, устанавливает его
func checkFieldDefValue(field *entity.Field,
	env *object.Environment,
	hash *object.Hash) error {
	defValExpr, err := field.Configs.Get("defValue")
	if err != nil {
		return fmt.Errorf("Field %s: %s", field.Name, err.Error())
	}

	if (defValExpr == nil) && field.NonNullable {
		return fmt.Errorf("Value for the non nullable field %s is not set", field.Name)
	}

	if defValExpr == nil {
		return nil
	}

	var defVal = Eval(defValExpr, env)
	if defVal.Type() == object.ERROR_OBJ {
		return fmt.Errorf("Field %s, evaluting default value get error: %s", field.Name, defVal.(*object.Error).Message)
	}

	hashKey, err := object.GetHashKey(&object.String{
		Value: field.Name,
	})

	if err != nil {
		return fmt.Errorf("Field %s: %s", field.Name, err.Error())
	}

	hash.Pairs[hashKey] = object.HashPair{
		Key: &object.String{
			Value: field.Name,
		},
		Value: defVal,
	}
	return nil
}

//проверяет значение на соответствие параметрам поля
func СheckForCorrectly(val interface{}, conf entity.Config, env *object.Environment) error {
	switch cVal := val.(type) {
	case string:
		strConf, ok := conf.(*entity.StrConf)
		if !ok {
			return fmt.Errorf("Can't convert configs for string field")
		}
		return checkStringField(cVal, strConf, env)
	case int64:
		intConf, ok := conf.(*entity.IntConf)
		if !ok {
			return fmt.Errorf("Can't convert configs for integer field")
		}
		return checkIntField(cVal, intConf, env)
	case bool:
		boolConf, ok := conf.(*entity.BoolConf)
		if !ok {
			return fmt.Errorf("Can't convert configs for integer field")
		}
		return checkBoolField(cVal, boolConf, env)
	default:
		return fmt.Errorf("Undefined type of value %v", val)
	}

}

//переводит из object в тип поля
func convValue(obj object.Object,
	field *entity.Field) (interface{}, error) {
	switch field.FieldType.Type {
	case token.STRING_TYPE:
		val, err := object.ConvToString(obj)
		if err != nil {
			return nil, fmt.Errorf("field '%s': %s", field.Name, err.Error())
		}

		return val, nil
	case token.INT_TYPE:
		val, err := object.ConvToInt(obj)
		if err != nil {
			return nil, fmt.Errorf("field '%s': %s", field.Name, err.Error())
		}
		return val, nil
	case token.BOOL_TYPE:
		val, err := object.ConvToBool(obj)
		if err != nil {
			return nil, fmt.Errorf("field '%s': %s", field.Name, err.Error())
		}
		return val, nil
	default:
		return nil, fmt.Errorf("'%s' undefined type of field '%v'", field.Name, field.FieldType.Type.ToString())
	}
}

//Сливает два хеша в один
func mergeHash(base []object.Hash, update *object.Hash) []object.Hash {
	var res []object.Hash
	for _, val := range base {
		cval := val
		for uKey, uPair := range update.Pairs {
			cval.Pairs[uKey] = uPair
		}
		res = append(res, cval)
	}

	return res
}

//проверка на соответствие параметрам строкового поля
func checkStringField(val string, str *entity.StrConf, env *object.Environment) error {
	if str.MaxLen != nil {
		maxLenObj := Eval(str.MaxLen, env)
		if maxLenObj.Type() == object.ERROR_OBJ {
			return cantEvalParamError("maxLen", maxLenObj.Inspect())
		}
		maxLen, err := object.ConvToInt(maxLenObj)
		if err != nil {
			return fmt.Errorf("Can't convert maxLen value to int, returned error: %s", err.Error())
		}
		if int64(len([]rune(val))) > maxLen {
			return fmt.Errorf("Field string size more than max length %v", maxLen)
		}
	}

	if str.MinLen != nil {
		minLenObj := Eval(str.MinLen, env)
		if minLenObj.Type() == object.ERROR_OBJ {
			return cantEvalParamError("minLen", minLenObj.Inspect())
		}
		minLen, err := object.ConvToInt(minLenObj)
		if err != nil {
			return fmt.Errorf("Can't convert minLen value to int, returned error: %s", err.Error())
		}
		if int64(len([]rune(val))) < minLen {
			return fmt.Errorf("Field string size less than max length %v", minLen)
		}
	}

	return nil
}

//проверка на соответствие параметрам целочисленного поля
func checkIntField(val int64, conf *entity.IntConf, env *object.Environment) error {
	if conf.MaxValue != nil {
		maxValObj := Eval(conf.MaxValue, env)
		if maxValObj.Type() == object.ERROR_OBJ {
			return cantEvalParamError("maxValue", maxValObj.Inspect())
		}
		maxVal, err := object.ConvToInt(maxValObj)
		if err != nil {
			return fmt.Errorf("Can't convert maxValue value to int, returned error: %s", err.Error())
		}
		if val > maxVal {
			return fmt.Errorf("Field int value more than max value %v", maxVal)
		}
	}

	if conf.MinValue != nil {
		minValObj := Eval(conf.MinValue, env)
		if minValObj.Type() == object.ERROR_OBJ {
			return cantEvalParamError("minValue", minValObj.Inspect())
		}
		minVal, err := object.ConvToInt(minValObj)
		if err != nil {
			return fmt.Errorf("Can't convert minValue value to int, returned error: %s", err.Error())
		}
		if val < minVal {
			return fmt.Errorf("Field int value less than min value %v", minVal)
		}
	}
	return nil
}

//проверка на соответствие параметрам логического поля
func checkBoolField(val bool, conf *entity.BoolConf, env *object.Environment) error {
	return nil
}

//Возвращает параметры подключения
func getDB(model *entity.Model) (*dbms.DB, error) {
	var address string
	var db string
	var login string
	var pass string
	var err error
	DBConf := model.Configs.GetEmbedConfig("database")
	Database, ok := DBConf.(*entity.Database)
	if !ok {
		return nil, fmt.Errorf("Can't get database configs")
	}
	if Database.Address != nil {
		addressObj := Eval(Database.Address, object.NewEnvironment())
		if addressObj.Type() == object.ERROR_OBJ {
			return nil, cantEvalParamError("address", addressObj.Inspect())
		}
		address, err = object.ConvToString(addressObj)
		if err != nil {
			return nil, fmt.Errorf("Can't convert address value, returned error: %s", err.Error())
		}
	} else {
		return nil, fmt.Errorf("DB address value not set")
	}

	if Database.Driver != nil {
		dbObj := Eval(Database.Driver, object.NewEnvironment())
		if dbObj.Type() == object.ERROR_OBJ {
			return nil, cantEvalParamError("DBMS", dbObj.Inspect())
		}
		db, err = object.ConvToString(dbObj)
		if err != nil {
			return nil, fmt.Errorf("Can't convert driver value, returned error: %s", err.Error())
		}
	} else {
		return nil, fmt.Errorf("DB driver name not set")
	}

	if Database.Login != nil {
		loginObj := Eval(Database.Driver, object.NewEnvironment())
		if loginObj.Type() == object.ERROR_OBJ {
			return nil, cantEvalParamError("login", loginObj.Inspect())
		}
		login, err = object.ConvToString(loginObj)
		if err != nil {
			return nil, fmt.Errorf("Can't convert login value, returned error: %s", err.Error())
		}
	}

	if Database.Password != nil {
		passObj := Eval(Database.Driver, object.NewEnvironment())
		if passObj.Type() == object.ERROR_OBJ {
			return nil, cantEvalParamError("pass", passObj.Inspect())
		}

		pass, err = object.ConvToString(passObj)
		if err != nil {
			return nil, fmt.Errorf("Can't convert password value, returned error: %s", err.Error())
		}
	}

	return &dbms.DB{
		Project:  model.Project,
		Model:    model.Name,
		Address:  address,
		DBMS:     db,
		Login:    login,
		Password: pass,
	}, nil
}

func cantEvalParamError(paramName string, err string) error {
	return fmt.Errorf("Can't evaluated %s params, get error: %s", paramName, err)
}

/*
func checkFieldsVals(params map[object.HashKey]object.HashPair,
	fields map[string]entity.Field) (map[string]interface{}, error) {

	var res map[string]interface{}

	for mKey, mVal := range fields {
		var val object.Object

		var keyCode, err = object.GetHashKey(
			&object.String{
				Value: mKey,
			},
		)
		hashPair, ok := params[keyCode]

		if !ok {

		} else {
			val = hashPair.Value
		}

		switch mVal.FieldType.Type {
		case token.STRING:
			conv, err := convertToString()
			if err != nil {
				return res, fmt.Errorf("Field %s, error: %s", mKey, err.Error())
			}
			maxLen, err := mVal.Configs.Get("maxLen")
			if err != nil {
				return res, fmt.Errorf("Field %s: %s", mKey, err.Error())
			}

		}
	}
	return nil
}
*/
