package main

import (
	"Clerk/cmd"
	"log"

	"github.com/spf13/viper"
)

func main() {
	viper.SetConfigFile("config.yaml")
	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		log.Fatalf("Fatal error config file: %s \n", err)
	}
	viper.WatchConfig()

	cmd.Execute()
}
